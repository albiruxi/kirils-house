//
//  StaysModel.swift
//  Kiril's House
//
//  Created by ALBA VILA on 26/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class StaysModel: NSObject {
    var title : String = ""
    var descriptionStay: String = ""
    var photo : String = ""
    
    override init() {
        super.init()
    }

    func parseJson(dictionary: NSDictionary){
        self.title = "\(dictionary["title"]!)"
        self.descriptionStay = "\(dictionary["descriptionStay"]!)"
        self.photo = "\(dictionary["photo"]!)"
    }
}
