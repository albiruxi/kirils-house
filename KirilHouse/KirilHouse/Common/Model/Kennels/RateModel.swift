//
//  RateModel.swift
//  Kiril's House
//
//  Created by ALBA VILA on 26/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class RateModel: NSObject {
    var photo : String = ""
    
    override init() {
        super.init()
    }
    
    func parseJson(dictionary: NSDictionary){
        self.photo = "\(dictionary["photo"]!)"
    }

}
