//
//  PlayModel.swift
//  Kiril's House
//
//  Created by ALBA VILA on 26/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class PlayModel: NSObject {
    var titleLabelPlay : String = ""
    var imagePlay : String = ""
    
    override init() {
        super.init()
    }

    func parseJson(dictionary: NSDictionary){
        self.titleLabelPlay = "\(dictionary["titleLabelPlay"]!)"
        self.imagePlay = dictionary["imagePlay"] as! String
    }
}
