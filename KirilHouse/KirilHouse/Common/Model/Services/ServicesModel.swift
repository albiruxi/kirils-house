//
//  ServicesModel.swift
//  Kiril's House
//
//  Created by ALBA VILA on 27/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class ServicesModel: NSObject {
    var title : String = ""
    var photo : String = ""
    var idService: String = ""
    var type : String = ""
    
    override init() {
        super.init()
    }
    
    func parseJson(dictionary: NSDictionary){
        self.title = "\(dictionary["title"]!)"
        self.photo = "\(dictionary["photo"]!)"
        self.idService = "\(dictionary["idService"]!)"
        self.type = "\(dictionary["type"]!)"
    }

}
