//
//  PromotionsModel.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class PromotionsModel: NSObject {
    var imagePromotions: String = ""
    
    override init() {
        super.init()
    }
    
    func parseJson(dictionary: NSDictionary){
        self.imagePromotions = "\(dictionary["imagePromotions"]!)"

    }
}
