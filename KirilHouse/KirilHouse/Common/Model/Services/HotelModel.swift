//
//  HotelModel.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class HotelModel: NSObject {
    var rooms : String = ""
    var dateEntry : String = ""
    var dateDeparture: String = ""
    var name : String = ""
    var email : String = ""
    var phone : String = ""
    var sms : String = ""
    
    override init() {
        super.init()
    }
    
    func parseJson(dictionary: NSDictionary) {
        self.rooms = "\(dictionary["rooms"]!)"
        self.dateEntry = "\(dictionary["dateEntry"]!)"
        self.dateDeparture = "\(dictionary["dateDeparture"]!)"
        self.name = "\(dictionary["name"]!)"
        self.email = "\(dictionary["email"]!)"
        self.phone = "\(dictionary["phone"]!)"
        self.sms = "\(dictionary["sms"]!)"
    }
}



