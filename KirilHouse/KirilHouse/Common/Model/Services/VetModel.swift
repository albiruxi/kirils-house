//
//  VetModel.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class VetModel: NSObject {
    var service : String = ""
    var date : String = ""
    var dni : String = ""
    var phone : String = ""
    var sms : String = ""
    
    override init() {
        super.init()
    }
    
    func parseJson(dictionary: NSDictionary){
        self.service = "\(dictionary["service"]!)"
        self.date = "\(dictionary["date"]!)"
        self.dni = "\(dictionary["dni"]!)"
        self.phone = "\(dictionary["phone"]!)"
        self.sms = "\(dictionary["sms"]!)"
    }

}
