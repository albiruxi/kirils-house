//
//  TaxiModel.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class TaxiModel: NSObject {
    var name : String = ""
    var adress : String = ""
    var postalCode : String = ""
    var numDogs : String = ""
    var date : String = ""
    var sms : String = ""
    
    override init() {
        super.init()
    }
    
    func parseJson(dictionary: NSDictionary){
        self.name = "\(dictionary["name"]!)"
        self.adress = "\(dictionary["adress"]!)"
        self.postalCode = "\(dictionary["postalCode"]!)"
        self.numDogs = "\(dictionary["numDogs"]!)"
        self.date = "\(dictionary["date"]!)"
        self.sms = "\(dictionary["sms"]!)"
    }
    
}



