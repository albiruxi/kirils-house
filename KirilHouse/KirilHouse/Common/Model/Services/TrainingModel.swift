//
//  TrainingModel.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class TrainingModel: NSObject {
    var course : String = ""
    var phone : String = ""
    var email : String = ""
    var sms : String = ""
  
    override init() {
        super.init()
    }
    
    func parseJson(dictionary: NSDictionary) {
        self.course = "\(dictionary["course"]!)"
        self.phone = "\(dictionary["phone"]!)"
        self.email = "\(dictionary["email"]!)"
        self.sms = "\(dictionary["sms"]!)"
    }
}
