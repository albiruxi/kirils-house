//
//  HairdressingModel.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class HairdressingModel: NSObject {
    var services : String = ""
    var date : String = ""
    var time: String = ""
    var name : String = ""
    var email : String = ""
    var phone : String = ""
    var sms : String = ""
    
    override init() {
        super.init()
    }
    
    func parseJson(dictionary: NSDictionary) {
        self.services = "\(dictionary["services"]!)"
        self.date = "\(dictionary["date"]!)"
        self.time = "\(dictionary["time"]!)"
        self.name = "\(dictionary["name"]!)"
        self.email = "\(dictionary["email"]!)"
        self.phone = "\(dictionary["phone"]!)"
        self.sms = "\(dictionary["sms"]!)"
    }
}




