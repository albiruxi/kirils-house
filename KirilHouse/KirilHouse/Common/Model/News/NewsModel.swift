//
//  NewsModel.swift
//  Kiril's House
//
//  Created by ALBA VILA on 26/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class NewsModel: NSObject {
    var titleNews : String = ""
    var descriptionNews : String = ""
    var imageNews : String = ""
    
    override init() {
        super.init()
    }
    
    func parseJson(dictionary: NSDictionary){
        self.titleNews = "\(dictionary["titleNews"]!)"
        self.descriptionNews = "\(dictionary["descriptionNews"]!)"
        self.imageNews = "\(dictionary["imageNews"]!)"
    
    }

}
