//
//  VideosModel.swift
//  Kiril's House
//
//  Created by ALBA VILA on 1/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class VideosModel: NSObject {
    var titleVideo : String = ""
    var url : String = ""
    
    
    override init() {
        super.init()
    }
    
    func parseJson(dictionary: NSDictionary){
        self.titleVideo = "\(dictionary["titleVideo"]!)"
        self.url = "\(dictionary["url"]!)"
      
    }
    
}



