//
//  ContactModel.swift
//  Kiril's House
//
//  Created by ALBA VILA on 27/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class ContactModel: NSObject {
    var name : String = ""
    var phone : String = ""
    var mail : String = ""
    var matter : String = ""
    var sms : String = ""
    
    override init() {
        super.init()
    }
    
    func parseJson(dictionary: NSDictionary) {
        self.name = "\(dictionary["name"]!)"
        self.phone = "\(dictionary["phone"]!)"
        self.mail = "\(dictionary["mail"]!)"
        self.matter = "\(dictionary["matter"]!)"
        self.sms = "\(dictionary["sms"]!)"
    }
}
