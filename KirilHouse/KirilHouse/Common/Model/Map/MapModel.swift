//
//  MapModel.swift
//  Kiril's House
//
//  Created by ALBA VILA on 27/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class MapModel: NSObject {
    var name : String = ""
    var descr : String = ""
    var photo : String = ""
    var map : String = ""
    
    override init() {
        super.init()
    }
    
    func parseJson(dictionary: NSDictionary){
        self.name = "\(dictionary["name"]!)"
        self.descr = "\(dictionary["description"]!)"
        self.photo = "\(dictionary["photo"]!)"
        self.map = "\(dictionary["map"]!)"
        
    }


}
