//
//  DogsModel.swift
//  Kiril's House
//
//  Created by ALBA VILA on 26/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class DogsModel: NSObject {
    var idDogs : String = ""
    var url : String = ""
    
    override init() {
        super.init()
    }
    
    func parseJson(dictionary: NSDictionary) {
        self.idDogs = "\(dictionary["id"]!)"
        self.url = "\(dictionary["url"]!)"
    }
}
