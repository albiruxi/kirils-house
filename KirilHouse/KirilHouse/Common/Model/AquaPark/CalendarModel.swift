//
//  CalendarModel.swift
//  Kiril's House
//
//  Created by ALBA VILA on 27/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class CalendarModel: NSObject {
    var imageCalendar : String = ""
    var imageRate : String = ""
    
    override init() {
        super.init()
    }
    
    func parseJson(dictionary: [String: AnyObject]){
        self.imageCalendar = "\(dictionary["imageCalendar"]!)"
        self.imageRate = "\(dictionary["imageRate"]!)"
        
    }
}


