//
//  QuestionModel.swift
//  Kiril's House
//
//  Created by ALBA VILA on 27/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class QuestionModel: NSObject {
    var question : String = ""
    var answer : String = ""
    
    override init() {
        super.init()
    }
    
    func parseJson(dictionary: NSDictionary){
        self.question = "\(dictionary["question"]!)"
        self.answer = "\(dictionary["answer"]!)"
        
    }
}


