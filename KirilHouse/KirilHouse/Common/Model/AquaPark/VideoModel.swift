//
//  VideoModel.swift
//  Kiril's House
//
//  Created by ALBA VILA on 27/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class VideoModel: NSObject {
    var title : String = ""
    var video : String = ""
    
    override init() {
        super.init()
    }
    
    func parseJson(dictionary: NSDictionary){
        self.title = "\(dictionary["title"]!)"
        self.video = "\(dictionary["video"]!)"
        
    }
}

