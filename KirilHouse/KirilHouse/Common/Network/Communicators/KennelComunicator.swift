//
//  KennelComunicator.swift
//  Kiril's House
//
//  Created by ALBA VILA on 26/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

import Alamofire

enum StaysListCommunicatorResponse{
    case Success(StaysModel?)
    case Error(NSError)
}

enum PlayListComunicatorResponse{
        case Success([PlayModel]?)
        case Error(NSError)
}

enum RateListComunicatorResponse{
    case Success(RateModel?)
    case Error(NSError)
}


protocol PKennelComunicator{
    func getListStays(section:String, completionHandler: (StaysListCommunicatorResponse) ->Void)
    func getListPlay(completionHandler: (PlayListComunicatorResponse) ->Void)
    func getListRates(completionHandler: (RateListComunicatorResponse) ->Void)
}


class KennelComunicator: NSObject {
    func getListStays(seasons:String, completionHandler: (StaysListCommunicatorResponse) ->Void){
        Alamofire.request(
            .GET,
            "http://private-63ef2-kirilshouse.apiary-mock.com/stays/"+seasons)
            .validate()
            .responseJSON { (response) -> Void in
                
                if (response.result.isSuccess) {
                    let json = response.result.value as! [String: AnyObject]
                    let stay = StaysModel()
                    stay.parseJson(json)
                    completionHandler(.Success(stay))
                }
                else {
                    print("ERROR: \(response.result.error?.localizedDescription)")                  
                }
        }
    }
    
    func getListPlay(completionHandler: (PlayListComunicatorResponse) ->Void){ 
        Alamofire.request(
            .GET,
            "http://private-63ef2-kirilshouse.apiary-mock.com/listPlay")
            .validate()
            .responseJSON { (response) -> Void in
                
                if (response.result.isSuccess) {
                    let json = response.result.value as! [String: AnyObject]
                    let listPlay = json["listPlay"] as! [[String: AnyObject]]
                    
                    //-Parse Response -
                    var listPlayResponse = [PlayModel]()
                    for playsDic in listPlay{
                        let plays = PlayModel()
                        plays.parseJson(playsDic)
                        listPlayResponse.append(plays)
                    }
                    completionHandler(.Success(listPlayResponse))
                }
                else{
                    print("getListPlays ----> \(response.result.error?.localizedDescription)")
                }
        }
    }

    func getListRates(completionHandler: (RateListComunicatorResponse) ->Void){
        
        Alamofire.request(
            .GET,
            "http://private-63ef2-kirilshouse.apiary-mock.com/rates")
            .validate()
            .responseJSON { (response) -> Void in
                
                if (response.result.isSuccess) {
                    let json = response.result.value as! [String: AnyObject]
                    let rate = RateModel()
                    rate.parseJson(json)
                    completionHandler(.Success(rate))
                    
                }
                else {
                    print("ERROR: \(response.result.error?.localizedDescription)")
                }
        }
    }
}

