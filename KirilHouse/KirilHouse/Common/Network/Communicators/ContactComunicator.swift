//
//  ContactComunicator.swift
//  Kiril's House
//
//  Created by ALBA VILA on 27/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

import Alamofire

enum ContactCommunicatorResponse {
    case Success
    case Error(NSError)
}

protocol PContactComunicator {
    func sendContact(section:String, completionHandler: (ContactCommunicatorResponse) -> Void)
}

class ContactComunicator: NSObject {
    func sendContact(contactM:ContactModel, completionHandler: (ContactCommunicatorResponse) -> Void) {
        
        var dicContact = [String: String]()
        dicContact["name"] = contactM.name
        dicContact["phone"] = contactM.phone
        dicContact["mail"] = contactM.mail
        dicContact["matter"] = contactM.matter
        dicContact["sms"] = contactM.sms
       
        
        Alamofire.request(
            .POST,
            "http://private-63ef2-kirilshouse.apiary-mock.com/contact",
            parameters: dicContact)
            .responseJSON {  (response) -> Void in
                
                if (response.result.isSuccess) {
                    completionHandler(.Success)
                }
                else{
                    print("getListContact ----> \(response.result.error?.localizedDescription)")
                    let errorService = NSError(domain: (response.result.error?.domain)!, code: (response.result.error?.code)!, userInfo: response.result.error?.userInfo)
                    completionHandler(.Error(errorService))
                }
        }
    }
}

