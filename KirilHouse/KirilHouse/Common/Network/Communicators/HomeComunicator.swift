//
//  HomeComunicator.swift
//  Kiril's House
//
//  Created by ALBA VILA on 26/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

import Alamofire

enum HomeListCommunicatorResponse {
    case Success([DogsModel]?)
    case Error(NSError)
}

protocol PHomeListComunicator{
    func getListDogs(section:String, completionHandler: (HomeListCommunicatorResponse) -> Void)
}

class HomeComunicator: NSObject {
    func getListDogs(section:String, completionHandler: (HomeListCommunicatorResponse) -> Void){
        Alamofire.request(
            .GET,
            "http://private-63ef2-kirilshouse.apiary-mock.com/listDogs")
            .validate()
            .responseJSON { (response) -> Void in
                
                if (response.result.isSuccess) {
                    let json = response.result.value as! [String: AnyObject]
                    let listDogs = json["listDogs"] as! [[String: AnyObject]]
                    
                    //-Parse Response -
                    var listDogsResponse = [DogsModel]()
                    for dogDic in  listDogs{
                        let dog = DogsModel()
                        dog.parseJson(dogDic)
                        listDogsResponse.append(dog)
                    }
                    completionHandler(.Success(listDogsResponse))
                }
                else {
                    //completionHandler(.Error(response.result.error)!)
                }
        }
    }
}
