//
//  ServicesComunicator.swift
//  Kiril's House
//
//  Created by ALBA VILA on 27/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//


import UIKit

import Alamofire

enum VetCommunicatorResponse {
    case Success()
    case Error(NSError)
}

enum TrainingCommunicatorResponse {
    case Success()
    case Error(NSError)
}

enum HotelCommunicatorResponse {
    case Success()
    case Error(NSError)
}

enum TaxiCommunicatorResponse {
    case Success()
    case Error(NSError)
}

enum HairdressingCommunicatorResponse {
    case Success()
    case Error(NSError)
}

enum OpinionCommunicatorResponse {
    case Success()
    case Error(NSError)
}
enum PromotionsListCommunicatorResponse {
    case Success([PromotionsModel]?)
    case Error(NSError)
}




protocol PServicesListComunicator{
    func sendContactVet(vetM: VetModel, completionHandler: (VetCommunicatorResponse) ->Void)
    func sendContactTraining(trainingM: TrainingModel, completionHandler : (TrainingCommunicatorResponse) -> Void)
    func sendContactHotel(hotelM: HotelModel, completionHandler : (HotelCommunicatorResponse) -> Void)
    func sendContactTaxi(taxiM: TaxiModel, completionHandler : (TaxiCommunicatorResponse) -> Void)
    func sendContactHairdressing(hairdressingM: HairdressingModel, completionHandler : (HairdressingCommunicatorResponse) -> Void)
    func sendContactOpinion(opinionM: OpinionModel, completionHandler : (OpinionCommunicatorResponse) -> Void)
    func getListPromotions(completionHandler: (PromotionsListCommunicatorResponse) -> Void)
}


class ServicesComunicator: NSObject {

    func sendContactVet(vetM: VetModel, completionHandler: (VetCommunicatorResponse) ->Void){
        var dicVet = [String: String]()
        dicVet["service"] = vetM.service
        dicVet["date"] = vetM.date
        dicVet["dni"] = vetM.dni
        dicVet["phone"] = vetM.phone
        dicVet["sms"] = vetM.sms
        
        Alamofire.request(
            .POST,
            "http://private-63ef2-kirilshouse.apiary-mock.com/services/vet",
            parameters: dicVet)
            .responseJSON {  (response) -> Void in
                if (response.result.isSuccess) {
                    completionHandler(.Success())
                }
                else{
                    print("sendContactVet ----> \(response.result.error?.localizedDescription)")
                    let errorService = NSError(domain: (response.result.error?.domain)!, code: (response.result.error?.code)!, userInfo: response.result.error?.userInfo)
                    completionHandler(.Error(errorService))
                }
        }
    }
    
    func sendContactTraining(trainingM: TrainingModel, completionHandler : (TrainingCommunicatorResponse) -> Void){
        var dicTraining = [String: String]()
        dicTraining["course"] = trainingM.course
        dicTraining["phone"] = trainingM.phone
        dicTraining["email"] = trainingM.email
        dicTraining["sms"] = trainingM.sms
        
        Alamofire.request(
            .POST,
            "http://private-63ef2-kirilshouse.apiary-mock.com/services/training",
            parameters: dicTraining)
            .responseJSON {  (response) -> Void in
                if (response.result.isSuccess) {
                    completionHandler(.Success())
                }
                else{
                    print("sendContactTraining ----> \(response.result.error?.localizedDescription)")
                    let errorService = NSError(domain: (response.result.error?.domain)!, code: (response.result.error?.code)!, userInfo: response.result.error?.userInfo)
                    completionHandler(.Error(errorService))
                }
        }
    }

    
    func sendContactHotel(hotelM: HotelModel, completionHandler: (HotelCommunicatorResponse) ->Void){
        var dicHotel = [String: String]()
        dicHotel["rooms"] = hotelM.rooms
        dicHotel["entry"] = hotelM.dateEntry
        dicHotel["departure"] = hotelM.dateDeparture
        dicHotel["name"] = hotelM.name
        dicHotel["email"] = hotelM.email
        dicHotel["phone"] = hotelM.phone
        dicHotel["sms"] = hotelM.sms
        
        Alamofire.request(
            .POST,
            "http://private-63ef2-kirilshouse.apiary-mock.com/services/hotel",
            parameters: dicHotel)
            .responseJSON {  (response) -> Void in
                if (response.result.isSuccess) {
                    completionHandler(.Success())
                }
                else{
                    print("sendContactHotel ----> \(response.result.error?.localizedDescription)")
                    let errorService = NSError(domain: (response.result.error?.domain)!, code: (response.result.error?.code)!, userInfo: response.result.error?.userInfo)
                    completionHandler(.Error(errorService))
                }
        }
    }
    
    func sendContactTaxi(taxiM: TaxiModel, completionHandler: (TaxiCommunicatorResponse) ->Void){
        var dicTaxi = [String: String]()
        dicTaxi["name"] = taxiM.name
        dicTaxi["entry"] = taxiM.adress
        dicTaxi["departure"] = taxiM.postalCode
        dicTaxi["name"] = taxiM.numDogs
        dicTaxi["phone"] = taxiM.date
        dicTaxi["sms"] = taxiM.sms
        
        Alamofire.request(
            .POST,
            "http://private-63ef2-kirilshouse.apiary-mock.com/services/taxi",
            parameters: dicTaxi)
            .responseJSON {  (response) -> Void in
                if (response.result.isSuccess) {
                    completionHandler(.Success())
                }
                else{
                    print("sendContactTaxi ----> \(response.result.error?.localizedDescription)")
                    let errorService = NSError(domain: (response.result.error?.domain)!, code: (response.result.error?.code)!, userInfo: response.result.error?.userInfo)
                    completionHandler(.Error(errorService))
                }
        }
    }
    
    func sendContactHairdressing(hairdressingM: HairdressingModel, completionHandler: (HairdressingCommunicatorResponse) ->Void){
        var dicHairdressing = [String: String]()
        dicHairdressing["services"] = hairdressingM.services
        dicHairdressing["date"] = hairdressingM.date
        dicHairdressing["time"] = hairdressingM.time
        dicHairdressing["name"] = hairdressingM.name
        dicHairdressing["email"] = hairdressingM.email
        dicHairdressing["phone"] = hairdressingM.phone
        dicHairdressing["sms"] = hairdressingM.sms
        
        Alamofire.request(
            .POST,
            "http://private-63ef2-kirilshouse.apiary-mock.com/services/hairdressing",
            parameters: dicHairdressing)
            .responseJSON {  (response) -> Void in
                if (response.result.isSuccess) {
                    completionHandler(.Success())
                }
                else{
                    print("sendContactHairdressing ----> \(response.result.error?.localizedDescription)")
                    let errorService = NSError(domain: (response.result.error?.domain)!, code: (response.result.error?.code)!, userInfo: response.result.error?.userInfo)
                    completionHandler(.Error(errorService))
                }
        }
    }
    
    func sendContactOpinion(opinionM: OpinionModel, completionHandler: (OpinionCommunicatorResponse) ->Void){
        var dicOpinion = [String: String]()
        dicOpinion["opinion"] = opinionM.opinion
    
        Alamofire.request(
            .POST,
            "http://private-63ef2-kirilshouse.apiary-mock.com/services/opinion",
            parameters: dicOpinion)
            .responseJSON {  (response) -> Void in
                if (response.result.isSuccess) {
                    completionHandler(.Success())
                }
                else{
                    print("sendContactOpinion ----> \(response.result.error?.localizedDescription)")
                    let errorService = NSError(domain: (response.result.error?.domain)!, code: (response.result.error?.code)!, userInfo: response.result.error?.userInfo)
                    completionHandler(.Error(errorService))
                }
        }
   }
    
        func getListPromotions(completionHandler: (PromotionsListCommunicatorResponse) -> Void){
            Alamofire.request(
                .GET,
                "http://private-63ef2-kirilshouse.apiary-mock.com/services/listPromotions")
                .validate()
                .responseJSON { (response) -> Void in
                    
                    if (response.result.isSuccess) {
                        let json = response.result.value as! [String: AnyObject]
                        let listPromotions = json["listPromotions"] as! [[String: AnyObject]]
                        
                        //-Parse Response -
                        var listPromotionsResponse = [PromotionsModel]()
                        for promotionsDic in listPromotions{
                            let promotions = PromotionsModel()
                            promotions.parseJson(promotionsDic)
                            listPromotionsResponse.append(promotions)
                        }
                        completionHandler(.Success(listPromotionsResponse))
                    }
                    else{
                        print("getListPromotions ----> \(response.result.error?.localizedDescription)")
                    }
                }
            }
        }

