//
//  MapsComunicator.swift
//  Kiril's House
//
//  Created by ALBA VILA on 27/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

enum MapsListCommunicatorResponse {
    case Success([MapModel]?)
    case Error(NSError)
}
protocol PMapsListComunicator{
    func getListMaps(section:String, completionHandler: (MapsListCommunicatorResponse) -> Void)
}

class MapsComunicator: NSObject {
    func getListMaps(section:String, completionHandler: (MapsListCommunicatorResponse) -> Void){
        let map = ["name":"nombre Krili's House", "description":"descripcion ","photo":"imagen", "map":"mapa ubicacion"]
        
        let listMaps = [map]
    
    
        //-Parse Response -
        var listMapsResponse = [MapModel]()
        for mapDic in listMaps {
            let map = MapModel()
            map.parseJson(mapDic)
            listMapsResponse.append(map)
        }
        completionHandler(.Success(listMapsResponse))
    }
}
