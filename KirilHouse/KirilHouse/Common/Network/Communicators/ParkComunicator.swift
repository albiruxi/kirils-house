//
//  ParkComunicator.swift
//  Kiril's House
//
//  Created by ALBA VILA on 27/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

import Alamofire


    enum CalendarListCommunicatorResponse{
        case Success([CalendarModel]?)
        case Error(NSError)
    }
    
    enum NormsListComunicatorResponse{
        case Success(NormsModel?)
        case Error(NSError)
    }
    
    enum QuestionsListComunicatorResponse{
        case Success([QuestionModel]?)
        case Error(NSError)
        
    }
    
    enum PlaneListCommunicatorResponse{
        case Success(PlaneModel?)
        case Error(NSError)
    }
    
    enum VideoListCommunicatorResponse{
        case Success(VideoModel?)
        case Error(NSError)
    }
    
    
    protocol PParkComunicator{
        func getListCalendar(section:String, completionHandler: (CalendarListCommunicatorResponse) ->Void)
        func getListNorm(section:String, completionHandler: (NormsListComunicatorResponse) ->Void)
        func getListQuestions(section:String, completionHandler: (QuestionsListComunicatorResponse) ->Void)
        func getListPlane(section:String, completionHandler: (PlaneListCommunicatorResponse) ->Void)
        func getListVideo(section:String, completionHandler: (VideoListCommunicatorResponse) ->Void)
    }
    
class ParkComunicator: NSObject {
    func getListCalendar(completionHandler: (CalendarListCommunicatorResponse) ->Void){
            Alamofire.request(
                .GET,
                "http://private-63ef2-kirilshouse.apiary-mock.com/aqua/listCalendar")
                .validate()
                .responseJSON { (response) -> Void in
                    
                  
                    if(response.result.isSuccess){
                        
                        let json = response.result.value as! [String: AnyObject]
                        let jsonListCalendar = json["listCaldendar"] as! [[String: AnyObject]]
                        
                        var listCalendar = [CalendarModel]()
                        for jsonCalendar in jsonListCalendar{
                            let calendar = CalendarModel()
                            calendar.parseJson(jsonCalendar)
                            listCalendar.append(calendar)
                        }
                        completionHandler(.Success(listCalendar))
                    }
                    else {
                        print("ERROR: \(response.result.error?.localizedDescription)")
                    }
            }
    }

        func getListNorms(completionHandler: (NormsListComunicatorResponse) ->Void){
            Alamofire.request(
                .GET,
                "http://private-63ef2-kirilshouse.apiary-mock.com/aqua/norms")
                .validate()
                .responseJSON { (response) -> Void in
                    
                    if(response.result.isSuccess){
                        let json = response.result.value as! [String: AnyObject]
                        let norms = NormsModel()
                        norms.parseJson(json)
                        completionHandler(.Success(norms))
                    }
                    else {
                        print("ERROR: \(response.result.error?.localizedDescription)")
                    }
            }
    }
    
        
        
    func getListQuestions(completionHandler: (QuestionsListComunicatorResponse) -> Void){
        Alamofire.request(
            .GET,
            "http://private-63ef2-kirilshouse.apiary-mock.com/aqua/listQuestions")
            .validate()
            .responseJSON { (response) -> Void in
                
                if (response.result.isSuccess) {
                    let json = response.result.value as! [String: AnyObject]
                    let listQuestions = json["listQuestions"] as! [[String: AnyObject]]
                    
                    //-Parse Response -
                    var listQuestionsResponse = [QuestionModel]()
                    for questionDic in listQuestions{
                        let questions = QuestionModel()
                        questions.parseJson(questionDic)
                        listQuestionsResponse.append(questions)
                    }
                    completionHandler(.Success(listQuestionsResponse))
                }
                else{
                    print("getListQuestions ----> \(response.result.error?.localizedDescription)")
                }
        }
    }

    
       

        
        func getListPlane(completionHandler: (PlaneListCommunicatorResponse) ->Void){
            Alamofire.request(
                .GET,
                "http://private-63ef2-kirilshouse.apiary-mock.com/aqua/plane")
                .validate()
                .responseJSON { (response) -> Void in
                    
                    if(response.result.isSuccess){
                        let json = response.result.value as! [String: AnyObject]
                        let plane = PlaneModel()
                        plane.parseJson(json)
                        completionHandler(.Success(plane))
                    }
                    else {
                        print("ERROR: \(response.result.error?.localizedDescription)")
                    }
            }
        }
        
        func getListVideo(section:String, completionHandler: (VideoListCommunicatorResponse) ->Void){
            Alamofire.request(
                .GET,
                "http://private-63ef2-kirilshouse.apiary-mock.com/park/norms/")
                .validate()
                .responseJSON { (response) -> Void in
                    
                    if(response.result.isSuccess){
                        let json = response.result.value as! [String: AnyObject]
                        let videos = VideoModel()
                        videos.parseJson(json)
                        completionHandler(.Success(videos))
                    }
                    else {
                        print("ERROR: \(response.result.error?.localizedDescription)")
                    }
                }
        }
}


