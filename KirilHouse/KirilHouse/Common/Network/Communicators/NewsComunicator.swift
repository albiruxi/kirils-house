//
//  NewsComunicator.swift
//  Kiril's House
//
//  Created by ALBA VILA on 27/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit
import Alamofire

enum NewsListCommunicatorResponse {
    case Success([NewsModel]?)
    case Error(NSError)
}

protocol PNewsListComunicator{
    func getListNews(completionHandler: (NewsListCommunicatorResponse) -> Void)
}


class NewsComunicator: NSObject {
    func getListNews(completionHandler: (NewsListCommunicatorResponse) -> Void){
        Alamofire.request(
            .GET,
            "http://private-63ef2-kirilshouse.apiary-mock.com/listNews")
            .validate()
            .responseJSON { (response) -> Void in
    
                if (response.result.isSuccess) {
                    let json = response.result.value as! [String: AnyObject]
                    let listNews = json["listNews"] as! [[String: AnyObject]]
                    
                    //-Parse Response -
                    var listNewsResponse = [NewsModel]()
                    for newsDic in listNews{
                        let news = NewsModel()
                        news.parseJson(newsDic)
                        listNewsResponse.append(news)
                    }
                    completionHandler(.Success(listNewsResponse))
                }
                else{
                    print("getListNews ----> \(response.result.error?.localizedDescription)")
                }
        }
    }
}