//
//  VideosComunicator.swift
//  Kiril's House
//
//  Created by ALBA VILA on 30/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit
import Alamofire

enum VideosListCommunicatorResponse {
    case Success([VideosModel]?)
    case Error(NSError)

}
    
protocol PVideosListComunicator{
    func getListSVideos(section:String, completionHandler: (VideosListCommunicatorResponse) -> Void)
}
    
class VideosComunicator: NSObject {
    func getListVideos(completionHandler: (VideosListCommunicatorResponse) -> Void){
        Alamofire.request(
            .GET,
            "http://private-63ef2-kirilshouse.apiary-mock.com/videos")
            .validate()
            .responseJSON { (response) -> Void in
                
                if (response.result.isSuccess) {
                    let json = response.result.value as! [String: AnyObject]
                    let listVideos = json["listvideos"] as! [[String: AnyObject]]
                    
                    //-Parse Response -
                    var listVideosResponse = [VideosModel]()
                    for videosDic in listVideos{
                        let videos = VideosModel()
                        videos.parseJson(videosDic)
                        listVideosResponse.append(videos)
                    }
                    completionHandler(.Success(listVideosResponse))
                }
                else{
                    print("\(response.result.error?.localizedDescription)")
                }
        }
    }
}