//
//  BaseViewController.swift
//  Kiril's House
//
//  Created by ALBA VILA on 27/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var spinner: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0,100,100))

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeSpinner()
    }
    
    func initializeSpinner() {
        
        self.spinner.center = self.view.center
        self.spinner.hidesWhenStopped = true
        self.spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.spinner.stopAnimating()
        self.spinner.backgroundColor = UIColor.clearColor()
        self.spinner.color = UIColor.blackColor()
        view.addSubview(spinner)
    }
    
    // MARK: -- Public Method --
    
    func spinner(show : Bool)
    {
        if (show)
        {
            self.spinner.startAnimating()
        }
        else
        {
            self.spinner.stopAnimating()
        }
    }
}
