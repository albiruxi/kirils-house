//
//  RegisterPresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 9/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit


protocol PRegisterPresenter{  
    func showErrorRegister(error: String)
    func showSuccess()
}

class RegisterPresenter: NSObject {
    var delegate: PRegisterPresenter?
    
    
    // MARK: Public Method
    
    func userDidRegister(email: String, pass: String) {
        let validateRegister = ValidateUser()
        validateRegister.validate(email, pass: pass)
        
        if (validateRegister.status) {
            self.saveUser(email, pass: pass)
            self.delegate?.showSuccess()
        }
        else {
            self.delegate?.showErrorRegister(validateRegister.responseError!)
        }
    }
        
    // MARK : Private Method
    
    func saveUser(email: String, pass: String) {
        let defaults = NSUserDefaults.standardUserDefaults()        
        //-- Recover --
        var arrayUsers = defaults.objectForKey("usersSaved") as? [[String:String]] ?? [[String:String]]()
        let dictUser = ["email": email, "pass":pass]
        arrayUsers.append(dictUser)
        defaults.setObject(arrayUsers, forKey: "usersSaved")
    }
}
