//
//  RegisterViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 9/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, PRegisterPresenter {
    
    //--OUTLET---
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passTextField: UITextField!

    
    //--VARIABLE--
    var presenter: RegisterPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.initializeView()
    }

    func initializeView() {
        self.presenter = RegisterPresenter()
        self.presenter?.delegate = self
    }
    
    // MARK : Actions
    
    @IBAction func sendAction(sender: AnyObject) {
         self.presenter?.userDidRegister(self.emailTextField.text!, pass: self.passTextField.text!)
    }
    
    // MARK : PRegisterPresenter
    
    func showErrorRegister(error: String) {
        self.view.endEditing(true)
        let alertController = UIAlertController(title: "Alerta", message: error, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showSuccess() {
        let alertController = UIAlertController(title: "Alerta", message: "Todo ha ido correctamente", preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            self.navigationController?.popViewControllerAnimated(true)
        }
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true,  completion: nil)
    }    
}
