//
//  ValidateUser.swift
//  KirilHouse
//
//  Created by ALBA VILA on 9/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class ValidateUser {    
    var status: Bool = false
    var responseError: String?    
    
    func validate(email: String, pass: String) {
        
        if (email.characters.count == 0) {
            status = false
            responseError = "El email no puede ser vacio";
        }
        else if (pass.characters.count == 0) {
            status = false
            responseError = "La contraseña no puede ser vacia";
        }
        else if (self.isIncorrectPass(pass)) {
            status = false
            responseError = "La contraseña debe tener mas de 5 caracteres";
        }
        else {
            status = true
        }
    }
    
    // MARK : Private Method
    
    private func isIncorrectPass(pass: String) -> Bool {
        if (pass.characters.count <= 5) {
            return true
        }
        else {
            return false
        }
    }
}
