//
//  LoginPresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 11/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

protocol PLoginPresenter{
    func showError(textError:String)
    func showUserFound()
}

class LoginPresenter: NSObject {
    var delegate: PLoginPresenter?
    
    func userDidTapLogin(email: String, password: String) {
        
        let validateRegister = ValidateUser()
        validateRegister.validate(email, pass: password)
        
        if (validateRegister.status) {
            
            let isExistUser = self.existUser(email, passwordLogin: password)
            
            if (isExistUser){
                self.delegate?.showUserFound()                
            }
            else {
                self.delegate?.showError("Usuario no encontrado")
            }
        }
        else {
           self.delegate?.showError(validateRegister.responseError!)
        }
    }
    
    // MARK : Private Method
    
    private func existUser(emailLogin: String, passwordLogin: String) -> Bool{
        let defaults = NSUserDefaults.standardUserDefaults()
        //-- Recover --
        let arrayUsers = defaults.objectForKey("usersSaved") as? [[String:String]] ?? [[String:String]]()
        
        var founded = false
        
        for user in arrayUsers {
            let email: String  = user["email"]!
            let pass: String  = user["pass"]!
            
            if (email == emailLogin && pass == passwordLogin) {
                founded = true
            }
        }
        
        return founded
    }
}


