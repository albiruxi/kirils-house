//
//  LoginViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 9/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//


import UIKit

class LoginViewController: UIViewController, PLoginPresenter {
    
    //-- IBOUTLET --
    @IBOutlet var botonMenu: UIBarButtonItem!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passTextField: UITextField!
    
    //--VARIABLE---
    var presenter : LoginPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.prepareMenu()
        
        self.inicialize()
    }
    
    func inicialize(){
        self.presenter = LoginPresenter()
        self.presenter?.delegate = self
    }
    
    // MARK: Private Method
    
    func prepareMenu() {
        if self.revealViewController() != nil{
            botonMenu.target = self.revealViewController()
            botonMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }   
    
    // MARK : PLoginPresenter

    func showUserFound() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var vc = UIViewController()
        vc = storyboard.instantiateViewControllerWithIdentifier("ServicesViewController") as! ServicesViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showError(textError: String) {
        self.view.endEditing(true)
        let alertController = UIAlertController(title: "Alerta", message: textError, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK : Actions
    
    @IBAction func access(sender: AnyObject) {
        var email: String
        email = ""
        email = self.emailTextField.text!
        
        let pass: String = self.passTextField.text!
        
        self.presenter?.userDidTapLogin(email, password: pass)
    }
}

 
 
