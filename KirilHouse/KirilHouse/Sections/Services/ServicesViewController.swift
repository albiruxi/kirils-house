//
//  ServiciosViewController.swift
//  Kiril's House
//
//  Created by ALBA VILA on 20/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class ServicesViewController: UIViewController, PServicesPresenter, UITableViewDelegate {


    @IBOutlet var tableView: UITableView!
    @IBOutlet var botonMenu: UIBarButtonItem!


    //--VARIABLE--//
    var presenter: ServicesPresenter?
    var listItemMenu: [ServicesModel]?

    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.prepareMenu()
    
        self.inicializeView()
    }

    func inicializeView(){
        self.presenter = ServicesPresenter()
        self.presenter?.delegate = self
        self.presenter?.mainViewIsready()
        
        self.navigationItem.title = "Servicios"
    }

    // MARK: Private Method
    
    func prepareMenu() {
        if self.revealViewController() != nil{
            botonMenu.target = self.revealViewController()
            botonMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    // MARK : PServicesPresenter
    
    func showServices(listServices: [ServicesModel]) {
        self.listItemMenu = listServices
        self.tableView.reloadData()
    }

    
    // MARK : Table Delegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listItemMenu!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let servicesModel = self.listItemMenu![indexPath.row]
        let cell:ServicesCell = tableView.dequeueReusableCellWithIdentifier("servicesCell") as! ServicesCell!
        cell.imageService.image = UIImage(named: servicesModel.photo)
        cell.titleLabel.text = servicesModel.title
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let servicesModel = self.listItemMenu![indexPath.row]

        if (servicesModel.type == "Training") {
            let vc = storyboard.instantiateViewControllerWithIdentifier("TrainingViewController") as! TrainingViewController
            vc.idService = servicesModel.idService
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if (servicesModel.type == "Hotel") {
            let vc = storyboard.instantiateViewControllerWithIdentifier("HotelViewController") as! HotelViewController
            vc.idServices = servicesModel.idService
            self.navigationController?.pushViewController(vc, animated: true)
        }
      
        if (servicesModel.type == "Hairdressing") {
            let vc = storyboard.instantiateViewControllerWithIdentifier("HairdressingViewController") as! HairdressingViewController
            vc.idService = servicesModel.idService
            self.navigationController?.pushViewController(vc, animated: true)
        }

       if (servicesModel.type == "Vet") {
           let vc = storyboard.instantiateViewControllerWithIdentifier("VetViewController") as! VetViewController
            vc.idServices = servicesModel.idService
            self.navigationController?.pushViewController(vc, animated: true)
        }

        if (servicesModel.type == "Taxi") {
            let vc = storyboard.instantiateViewControllerWithIdentifier("TaxiViewController") as! TaxiViewController
            vc.idService = servicesModel.idService
            self.navigationController?.pushViewController(vc, animated: true)
        }
      
        if (servicesModel.type == "Opinion") {
            let vc = storyboard.instantiateViewControllerWithIdentifier("OpinionViewController") as! OpinionViewController
            vc.idServices = servicesModel.idService
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if (servicesModel.type == "Promotions") {
            let vc = storyboard.instantiateViewControllerWithIdentifier("PromotionsViewController") as! PromotionsViewController
            vc.idServices = servicesModel.idService
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
 
}


        







   