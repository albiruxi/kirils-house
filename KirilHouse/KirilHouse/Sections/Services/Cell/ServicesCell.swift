//
//  ServicesCell.swift
//  KirilHouse
//
//  Created by ALBA VILA on 7/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class ServicesCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var imageService: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
