//
//  ServicesPresenter.swift
//  Kiril's House
//
//  Created by ALBA VILA on 30/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit


protocol PServicesPresenter {
    func showServices(listServices:[ServicesModel])
}

class ServicesPresenter: NSObject {
    var delegate: PServicesPresenter?
    var listServices = [ServicesModel]()
    
    override init() {
        super.init()
    }
    
    // MARK : Public Method
    
    func  mainViewIsready() {
        self.createMenu()
        self.delegate?.showServices(self.listServices)
    }

    // MARK : Private Method
    
    func createMenu() {
        let serviceMenu1 = ServicesModel()
        serviceMenu1.title = "Servicio adiestramiento"
        serviceMenu1.photo = "icon_menu_1"
        serviceMenu1.type = "Training"
        let serviceMenu2 = ServicesModel()
        serviceMenu2.title = "Hotel Canino"
        serviceMenu2.photo = "icon_menu_2"
        serviceMenu2.type = "Hotel"
        let serviceMenu3 = ServicesModel()
        serviceMenu3.title = "Servicio Peluqueria"
        serviceMenu3.photo = "icon_menu_3"
        serviceMenu3.type = "Hairdressing"
        let serviceMenu4 = ServicesModel()
        serviceMenu4.title = "Servicio Veterinario"
        serviceMenu4.photo = "icon_menu_4"
        serviceMenu4.type = "Vet"
        let serviceMenu5 = ServicesModel()
        serviceMenu5.title = "Servicio Taxi"
        serviceMenu5.photo = "icon_menu_5"
        serviceMenu5.type = "Taxi"
        let serviceMenu6 = ServicesModel()
        serviceMenu6.title = "Opiniones"
        serviceMenu6.photo = "icon_menu_6"
        serviceMenu6.type = "Opinion"
        let serviceMenu7 = ServicesModel()
        serviceMenu7.title = "Servicio Promociones"
        serviceMenu7.photo = "icon_menu_7"
        serviceMenu7.type = "Promotions"

        self.listServices = [serviceMenu1, serviceMenu2, serviceMenu3, serviceMenu4, serviceMenu5, serviceMenu6, serviceMenu7]
    }
}

