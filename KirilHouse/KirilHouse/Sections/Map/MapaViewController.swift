//
//  MapaViewController.swift
//  Kiril's House
//
//  Created by ALBA VILA on 24/2/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapaViewController: BaseViewController, PMapsPresenter {

    //-- IBOUTLET --
    @IBOutlet var menuBoton: UIBarButtonItem!
    @IBOutlet var Mapa: MKMapView!
    
    //-- Variable --
    var presenter: MapPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        //-- Init menu --
        self.prepareMenu()
        
        //-- Init view --
        self.initializeView()
    }
    
    // MARK : Initialize View
    
    func initializeView() {
        self.presenter = MapPresenter()
        self.presenter?.delegate = self
        self.presenter?.mainIsReady()
    }
    
    // MARK: Private Method
    
    func prepareMenu() {
        if self.revealViewController() != nil{
            menuBoton.target = self.revealViewController()
            menuBoton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    // MARK: PMapPresenter

    func showMap(resortAnnotation: MKPointAnnotation, region: MKCoordinateRegion) {
        self.Mapa.setRegion(region, animated: true)
        self.Mapa.addAnnotation(resortAnnotation)
    }
    
}

