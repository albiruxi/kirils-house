//
//  MapPresenter.swift
//  Kiril's House
//
//  Created by ALBA VILA on 28/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit
import MapKit


protocol PMapsPresenter {
    func showMap(resortAnnotation: MKPointAnnotation, region: MKCoordinateRegion)
}


class MapPresenter: NSObject {
    var delegate: PMapsPresenter?
    
    override init() {
        super.init()
    }
    
    // MARK: Public Method
    
    func mainIsReady() {        
        let latitude:CLLocationDegrees = 41.61642142539922
        let longitude:CLLocationDegrees = 2.303288650000013
        
        let latDelta:CLLocationDegrees = 0.01
        let longDelta:CLLocationDegrees = 0.01
        
        let theSpan:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, longDelta)
        let residence:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        
        let region:MKCoordinateRegion = MKCoordinateRegionMake(residence, theSpan)
        
        let resortAnnotation = MKPointAnnotation()
        resortAnnotation.coordinate = residence
        resortAnnotation.title = "Kiril's House"
        resortAnnotation.subtitle = "Resort canino"
        
        self.delegate?.showMap(resortAnnotation, region: region)
    }
}