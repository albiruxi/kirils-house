//
//  HairdressingViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit


class HairdressingViewController: UIViewController, PHairdressingPresenter, UIPickerViewDataSource, UIPickerViewDelegate {
    
    //---OUTLET
    @IBOutlet var textFieldService: UITextField!
    @IBOutlet var textFieldDate: UITextField!
    @IBOutlet var textFieldTime: UITextField!
    @IBOutlet var textFieldName: UITextField!
    @IBOutlet var textFieldEmail: UITextField!
    @IBOutlet var textFieldPhone: UITextField!
    @IBOutlet var textFieldSms: UITextField!
    
    
    //--VARIABLE
    var presenter: HairdressingPresenter?
    var idService: String?
    var services: [String]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicialize()

    }
    
    func inicialize() {
    self.navigationItem.title = "Peluqueria"
    
    self.createBarButton()
    
    self.presenter = HairdressingPresenter()
    self.presenter?.delegate = self
    self.presenter?.mainViewIsReady()
    }
    
    func createBarButton() {
    let barButton = UIBarButtonItem(title: "Enviar", style: .Plain , target: self, action: #selector(self.sendHairdressing))
    self.navigationItem.rightBarButtonItem = barButton
    }
    
    // MARK : Actions
    
    func sendHairdressing() {
        let HairdressingM = HairdressingModel()
        HairdressingM.services = self.textFieldService.text!
        HairdressingM.date = self.textFieldDate.text!
        HairdressingM.time = self.textFieldTime.text!
        HairdressingM.name = self.textFieldName.text!
        HairdressingM.email = self.textFieldEmail.text!
        HairdressingM.phone = self.textFieldPhone.text!
        HairdressingM.sms = self.textFieldSms.text!
        
        self.presenter?.sendHairdressing(HairdressingM)
    }

    // MARK: PHairdressingPresenter
    
    func showSuccess() {
        let alertController = UIAlertController(title: "Alerta", message: "Reserva en peluqueria realizada correctamente", preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            self.navigationController?.popViewControllerAnimated(true)
        }
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true,  completion: nil)
    }
    
  func showError(error: String) {
        self.view.endEditing(true)
        let alertController = UIAlertController(title: "Alerta", message: error, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showServices(services: [String]?) {
        self.services = services
        self.createPicker()
    }

    // MARK : Private Method
    
    func createPicker() {
        // PICKER
        let pickerViewService = UIPickerView()
        pickerViewService.delegate = self
        pickerViewService.backgroundColor = UIColor.whiteColor()
        self.textFieldService.inputView = pickerViewService
        
        // DATE PICKER
        let datePicker = UIDatePicker()
        datePicker.backgroundColor = UIColor.whiteColor()
        datePicker.tag = 0
        datePicker.datePickerMode = UIDatePickerMode.Date
        datePicker.addTarget(self, action: #selector(self.datePickerValueChanged), forControlEvents: UIControlEvents.ValueChanged)
        self.textFieldDate.inputView = datePicker
        
        // TIME PICKER
        let timePicker = UIDatePicker()
        timePicker.backgroundColor = UIColor.whiteColor()
        timePicker.tag = 1
        timePicker.datePickerMode = UIDatePickerMode.Time
        timePicker.addTarget(self, action: #selector(self.datePickerValueChanged), forControlEvents: UIControlEvents.ValueChanged)
        self.textFieldTime.inputView = timePicker
    }
    
    // MARK : PICKER DATE
    
    func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        
        if (sender.tag == 0) {
            self.textFieldDate.text = dateFormatter.stringFromDate(sender.date)
            self.textFieldDate.resignFirstResponder()
        }
        else {
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([NSCalendarUnit.Hour, NSCalendarUnit.Minute] , fromDate: sender.date)
            self.textFieldTime.text = "\(components.hour):\(components.minute)"
            self.textFieldTime.resignFirstResponder()
        }
    }

    // MARK: PickerView
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.services!.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.services![row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let servicesTemp = services![row]
        self.textFieldService.text = servicesTemp
        self.textFieldService.resignFirstResponder()
    }
}



