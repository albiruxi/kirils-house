//
//  HairdressingPresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//


import UIKit

protocol PHairdressingPresenter{
    func showSuccess()
    func showError(error: String)
    func showServices(services: [String]?)
}

class HairdressingPresenter: NSObject {
    var delegate: PHairdressingPresenter?
    var hairdressingComunicator: ServicesComunicator?
    var hairdressing = HairdressingModel()
    var services = ["Cortar", "Lavar", "Cortar y lavar", "Cortar uñas"]
    
    
    override init() {
        self.hairdressingComunicator = ServicesComunicator()
    }
    
    
    // MARK: Public Method
    
    func mainViewIsReady() {
        self.delegate?.showServices(self.services)
    }
    
    
    func sendHairdressing(hairdressingM: HairdressingModel) {
        
        let validator = ValidateServices()
        validator.validateHairdressing(hairdressingM)
        if (validator.status) {
            self.sendServiceHairdressing(hairdressingM)
        }
        else {
            self.delegate?.showError(validator.responseError!)
        }
    }
    
    // MARK : Private Method
     
    func sendServiceHairdressing(hairdressingM: HairdressingModel){
        self.hairdressingComunicator?.sendContactHairdressing(hairdressingM, completionHandler: { (response) in
            switch response {
            case .Success():
                self.delegate?.showSuccess()
            case .Error(let error):
                self.delegate?.showError(error.localizedDescription)
            }
        })
    }
}

