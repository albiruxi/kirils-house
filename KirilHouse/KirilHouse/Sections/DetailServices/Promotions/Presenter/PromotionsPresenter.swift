//
//  PromotionsPresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

protocol PPromotionsPresenter{
    func showPromotions(listPromotions:[PromotionsModel])
}

class PromotionsPresenter {
    var delegate: PPromotionsPresenter?
    var promotionsComunicator: ServicesComunicator?
    var listPromotions = [PromotionsModel]()
    
     init() {
        self.promotionsComunicator = ServicesComunicator()
    }
    
    // MARK: Public Method
    
    func mainViewIsReady(){
        self.promotionsComunicator?.getListPromotions({ (response) in
            switch (response) {
            case .Success(let listPromotions):
                self.delegate?.showPromotions(listPromotions!)
                break
            case .Error(let error):
                NSLog(error.localizedDescription, "")
                break
            }
        })
    }
}