//
//  PromotionsViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class PromotionsViewController: UIViewController, PPromotionsPresenter, UITableViewDataSource {
    
    //--OUTLET
    
    @IBOutlet var tableView: UITableView!
    
    //--VARIABLE
    var presenter: PromotionsPresenter?
    var idServices: String?
    var listPromotions = [PromotionsModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicialize()
    }
    
    func inicialize(){
        self.presenter = PromotionsPresenter()
        self.presenter?.delegate = self
        self.presenter?.mainViewIsReady()
        
        self.navigationItem.title = "Promociones"
    }
    
    // MARK: PPromotionsPresenter
    
    func showPromotions(listPromotions: [PromotionsModel]){
        self.listPromotions = listPromotions
        self.tableView.reloadData()
        
    }
    
    // MARK : TableViewDelegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listPromotions.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("promotionsCell", forIndexPath: indexPath) as!
        PromotionsCell
        
        let promotionsModel: PromotionsModel = self.listPromotions[indexPath.row]
        cell.imagePromotions.sd_setImageWithURL(NSURL(string: promotionsModel.imagePromotions))
        
        return cell
    }











}

 