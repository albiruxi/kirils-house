//
//  HotelPresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//


import UIKit

protocol PHotelPresenter{
    func showSuccess()
    func showError(error: String)
    func showRooms(rooms: [String]?)
}

class HotelPresenter: NSObject {
    var delegate: PHotelPresenter?
    var hotelComunicator: ServicesComunicator?
    var hotel = HotelModel()
    var rooms = ["Estancias de verano", "Estancias de invierno", "Junior Suite", "razas mini", "razas grandes"]
    
    override init(){
        self.hotelComunicator = ServicesComunicator()
    }
    
    // MARK: Public Method
    
    func mainViewIsReady() {
        self.delegate?.showRooms(self.rooms)
    }
    
    func sendHotel(hotelM: HotelModel) {
        
        let validator = ValidateServices()
        validator.validateHotel(hotelM)
        if (validator.status) {
            self.sendServiceHotel(hotelM)
        }
        else {
            self.delegate?.showError(validator.responseError!)
        }
    }
    
    // MARK : Private Method
    
    func sendServiceHotel(hotelM: HotelModel){
        self.hotelComunicator?.sendContactHotel(hotelM, completionHandler: { (response) in
            switch response {
            case .Success():
                self.delegate?.showSuccess()
            case .Error(let error):
                self.delegate?.showError(error.localizedDescription)
            }
        })
    }
}


