//
//  HotelViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//


import UIKit

class HotelViewController: UIViewController, PHotelPresenter, UIPickerViewDelegate, UIPickerViewDataSource {
    
    //--OUTLET
    @IBOutlet var textFieldRooms: UITextField!
    @IBOutlet var textFieldEntry: UITextField!
    @IBOutlet var textFieldDeparture: UITextField!
    @IBOutlet var textFieldName: UITextField!
    @IBOutlet var textFieldEmail: UITextField!
    @IBOutlet var textFieldPhone: UITextField!
    @IBOutlet var textFieldSms: UITextField!
    
    //-- Variable
    var idServices: String?
    var presenter: HotelPresenter?
    var rooms: [String]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicialize()

    }
    
    func inicialize(){
        self.navigationItem.title = "Hotel"
        
        self.createBarButton()
        
        self.presenter = HotelPresenter()
        self.presenter?.delegate = self
        self.presenter?.mainViewIsReady()
    }
    
    func createBarButton() {
        let barButton = UIBarButtonItem(title: "Enviar", style: .Plain , target: self, action: #selector(self.sendHotel))
        self.navigationItem.rightBarButtonItem = barButton
    }
    
   // MARK : Actions
    
    func sendHotel(){
        let hotelM = HotelModel()
        hotelM.rooms = self.textFieldRooms.text!
        hotelM.dateEntry = self.textFieldEntry.text!
        hotelM.dateDeparture = self.textFieldDeparture.text!
        hotelM.name = self.textFieldName.text!
        hotelM.email = self.textFieldEmail.text!
        hotelM.sms = self.textFieldSms.text!
        
        self.presenter?.sendHotel(hotelM)
    }
    
    // MARK: PHotelPresenter
    
    func showSuccess() {
        let alertController = UIAlertController(title: "Alerta", message: "Reserva de hotel realizada correctamente", preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            self.navigationController?.popViewControllerAnimated(true)
        }
    alertController.addAction(defaultAction)
    presentViewController(alertController, animated: true,  completion: nil)
    }
       
    func showError(error: String) {
        self.view.endEditing(true)
        let alertController = UIAlertController(title: "Alerta", message: error, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showRooms(rooms: [String]?) {
        self.rooms = rooms
        self.createPicker()
    }
    
    // MARK: PRIVATE METHOD
    
    func createPicker() {
        // PICKER
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.backgroundColor = UIColor.whiteColor()
        self.textFieldRooms.inputView = pickerView
        
        // DATE PICKERS
        self.createDatePicker(self.textFieldEntry, tag: 0)
        self.createDatePicker(self.textFieldDeparture, tag: 1)
    }
    
    func createDatePicker(textField: UITextField, tag: Int) {
        let datePickerEntry = UIDatePicker()
        datePickerEntry.backgroundColor = UIColor.whiteColor()
        datePickerEntry.tag = tag
        datePickerEntry.datePickerMode = UIDatePickerMode.Date
        textField.inputView = datePickerEntry
        datePickerEntry.addTarget(self, action: #selector(self.datePickerValueChanged), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    // MARK : PICKER DATE
    
    func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        if (sender.tag == 0) {
            self.textFieldEntry.text = dateFormatter.stringFromDate(sender.date)
            self.textFieldEntry.resignFirstResponder()
        }
        else {
            self.textFieldDeparture.text = dateFormatter.stringFromDate(sender.date)
            self.textFieldDeparture.resignFirstResponder()
        }
    }
    
    // MARK: PickerView
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.rooms!.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.rooms![row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let roomsTemp = rooms![row]
        self.textFieldRooms.text = roomsTemp
        self.textFieldRooms.resignFirstResponder()
    }
}


    


