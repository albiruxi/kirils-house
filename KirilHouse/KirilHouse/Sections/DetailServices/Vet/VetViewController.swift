//
//  VetViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 17/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class VetViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource , PVetPresenter{
    
    //OUTLET
    @IBOutlet var textFieldServices: UITextField!
    @IBOutlet var textFieldDate: UITextField!
    @IBOutlet var textFieldDni: UITextField!
    @IBOutlet var textFieldPhone: UITextField!
    @IBOutlet var textFieldSms: UITextField!
 
    //-- Variable
   
    var idServices: String?
    var presenter: VetPresenter?
    var services: [String]?
    var status: Bool = false
    var responseError: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicialize()
    }
    
    func inicialize() {
        self.navigationItem.title = "Veterinario"
        
        self.createBarButton()
        
        self.presenter = VetPresenter()
        self.presenter?.delegate = self
        self.presenter?.mainViewIsReady()
    }
    
    func createBarButton() {
        let barButton = UIBarButtonItem(title: "Enviar", style: .Plain , target: self, action: #selector(self.sendVet))
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    // MARK : Actions
    
    func sendVet() {
        let vetM = VetModel()
        vetM.service = self.textFieldServices.text!
        vetM.date = self.textFieldDate.text!
        vetM.dni = self.textFieldDni.text!
        vetM.phone = self.textFieldPhone.text!
        vetM.sms = self.textFieldSms.text!
     
        self.presenter?.sendVet(vetM)
    }
    
    // MARK: PVetPresenter
    
    func showServices(services: [String]?) {
        self.services = services
        self.createPicker()
    }
    
    func showSuccess() {
        let alertController = UIAlertController(title: "Alerta", message: "Reserva de veterinario realizada correctamente", preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            self.navigationController?.popViewControllerAnimated(true)
        }
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true,  completion: nil)
    }


    func showError(error: String) {
        self.view.endEditing(true)
        let alertController = UIAlertController(title: "Alerta", message: error, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
 
  
    // MARK : Private Method
    
    func createPicker() {
        // PICKER
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.backgroundColor = UIColor.whiteColor()
        self.textFieldServices.inputView = pickerView
        
        // DATE PICKER
        let datePicker = UIDatePicker()
        datePicker.backgroundColor = UIColor.whiteColor()
        datePicker.datePickerMode = UIDatePickerMode.Date
        self.textFieldDate.inputView = datePicker
        datePicker.addTarget(self, action: #selector(self.datePickerValueChanged), forControlEvents: UIControlEvents.ValueChanged)
    
    }

    // MARK : PICKER DATE
    
    func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        self.textFieldDate.text = dateFormatter.stringFromDate(sender.date)
        self.textFieldDate.resignFirstResponder()
    }
    
    // MARK: PickerView
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.services!.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.services![row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let servicesTemp = services![row]
        self.textFieldServices.text = servicesTemp
        self.textFieldServices.resignFirstResponder()
    }
}
    

