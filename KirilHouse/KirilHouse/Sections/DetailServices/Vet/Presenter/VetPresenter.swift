//
//  VetPresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 17/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//


import UIKit

protocol PVetPresenter{
    func showSuccess()
    func showError(error: String)
    func showServices(services: [String]?)
}

class VetPresenter: NSObject {
    var delegate: PVetPresenter?
    var vetComunicator: ServicesComunicator?
    var vet = VetModel()
    var services = ["Vacuna", "Revision", "Desparasitación", "Análisis clínico", "Radiología"]
    
    override init(){
       self.vetComunicator = ServicesComunicator()
    }

    // MARK: Public Method
    
    func mainViewIsReady() {
        self.delegate?.showServices(self.services)
    }
    
    func sendVet(vetM: VetModel) {
        
        let validator = ValidateServices()
        validator.validateVet(vetM)
        if (validator.status) {
            self.sendServiceVet(vetM)
        }
        else {
            self.delegate?.showError(validator.responseError!)
        }
    }
    
    // MARK : Private Method
    

    func sendServiceVet(vetM: VetModel){
        self.vetComunicator?.sendContactVet(vetM, completionHandler: { (response) in
            switch response {
            case .Success():
                self.delegate?.showSuccess()
            case .Error(let error):
                self.delegate?.showError(error.localizedDescription)
            }
        })
    }    
}
    

            