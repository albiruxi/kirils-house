//
//  TrainingViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit


class TrainingViewController: UIViewController, PTrainingPresenter, UIPickerViewDelegate {
    
    //--OUTLET
    @IBOutlet var courseTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var smsTextField: UITextField!
    
    
    //--VARIABLE
    var idService: String?
    var presenter: TrainingPresenter?
    var course: [String]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicialize()
    }
    
    func inicialize(){
        self.presenter = TrainingPresenter()
        self.presenter?.delegate = self
        self.presenter?.mainViewIsReady()
        
        self.navigationItem.title = "Adiestramiento"
        
        self.createBarButton()
        
    }
    
    func createBarButton(){
        let barButton = UIBarButtonItem(title: "Enviar", style: .Plain, target: self, action: #selector(self.sendTraining))
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    // MARK: ACTIONS
    
    func sendTraining(){
        let trainingM = TrainingModel()
        trainingM.course = self.courseTextField.text!
        trainingM.phone = self.phoneTextField.text!
        trainingM.email = self.emailTextField.text!
        trainingM.sms = self.smsTextField.text!
        
        self.presenter?.sendTraining(trainingM)
    }
    
    // MARK: PTrainingPresenter
 
    func showSuccess() {
        let alertController = UIAlertController(title: "Alerta", message: "Reserva de tu curso realizada correctamente", preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            self.navigationController?.popViewControllerAnimated(true)
        }
    alertController.addAction(defaultAction)
    presentViewController(alertController, animated: true,  completion: nil)
    }
    
    
    func showError(error: String) {
        self.view.endEditing(true)
        let alertController = UIAlertController(title: "Alerta", message: error, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showCourse(course: [String]?) {
        self.course = course
        self.createPicker()
    }
    

    //MARK : PRIVATE METHOD
    
    func createPicker(){
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.backgroundColor = UIColor.whiteColor()
        self.courseTextField.inputView = pickerView
    }
    
    // MARK: PICKERVIEW
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.course!.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.course![row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let courseTemp = course![row]
        self.courseTextField.text = courseTemp
        self.courseTextField.resignFirstResponder()
    }
}


