//
//  TrainingPresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

protocol PTrainingPresenter{
    func showSuccess()
    func showError(error: String)
    func showCourse(course: [String]?)
}

class TrainingPresenter: NSObject {
    var delegate: PTrainingPresenter?
    var trainingComunicator: ServicesComunicator?
    var training = TrainingModel()
    var courses = ["Curso cachorros","Obediencia básica","Terapia de comportamiento","Básico con correa","Agilidad", "Avanzado sin correa"]
    
    override init() {
        self.trainingComunicator = ServicesComunicator()
    }
    
    // MARK: Public Method
    
    func mainViewIsReady(){
        self.delegate?.showCourse(self.courses)
    }
    
    func sendTraining(trainingM: TrainingModel) {
        
        let validator = ValidateServices()
        validator.validateTraining(trainingM)
        if (validator.status) {
            self.sendServiceTraining(trainingM)
        }
        else {
            self.delegate?.showError(validator.responseError!)
        }
    }
    
    // MARK : Private Method
    
    func sendServiceTraining(trainingM: TrainingModel){
        self.trainingComunicator?.sendContactTraining(trainingM, completionHandler: { (response) in
            switch response {
            case .Success():
                self.delegate?.showSuccess()
            case .Error(let error):
                self.delegate?.showError(error.localizedDescription)
            }
        })
    }
}

