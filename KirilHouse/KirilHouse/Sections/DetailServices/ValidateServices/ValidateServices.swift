//
//  ValidateServices.swift
//  KirilHouse
//
//  Created by ALBA VILA on 1/6/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class ValidateServices {
    
    var status: Bool = false
    var responseError: String?
    
    func validateTaxi(taxiM :TaxiModel) {
        if (taxiM.name.characters.count == 0){
            status = false
            responseError = "El nombre no se puede enviar vacio";
        }
        else if (taxiM.adress.characters.count == 0){
            status = false
            responseError = "La dirección no puede enviar vacia";
        }
        else if (taxiM.postalCode.characters.count == 0){
            status = false
            responseError = "El codigo postal no puede enviar vacio";
        }
        else if (taxiM.numDogs.characters.count == 0){
            status = false
            responseError = "El número de perros no puede enviar vacio";
        }
        else if (taxiM.date.characters.count == 0){
            status = false
            responseError = "La fecha no puede enviar vacia";
        }
        else {
            status = true
        }
    }
        
    func validateHairdressing(hairdressingM :HairdressingModel) {
        if (hairdressingM.services.characters.count == 0){
            status = false
            responseError = "El servicio no se puede enviar vacio";
        }
        else if (hairdressingM.date.characters.count == 0){
            status = false
            responseError = "La fecha no se puede enviar vacia";
        }
        else if (hairdressingM.time.characters.count == 0){
            status = false
            responseError = "La hora no se puede enviar vacia";
        }
        else if (hairdressingM.name.characters.count == 0){
            status = false
            responseError = "El nombre no se puede enviar vacio";
        }
        else if (hairdressingM.email.characters.count == 0){
            status = false
            responseError = "El email no se puede enviar vacia";
        }
        else if (hairdressingM.phone.characters.count == 0){
            status = false
            responseError = "El teléfono no se puede enviar vacio";
        }
        else {
            status = true
        }
  }
    
    func validateOpinion(opinionM :OpinionModel) {
        if (opinionM.opinion.characters.count == 0){
            status = false
            responseError = "El comentario no se puede enviar vacio"
        }
        else {
            status = true
        }
    }
   
    func validateTraining(trainingM :TrainingModel) {
        if (trainingM.course.characters.count == 0){
            status = false
            responseError = "El curso no se puede enviar vacio";
        }
        else if (trainingM.phone.characters.count == 0){
            status = false
            responseError = "El teléfono no se puede enviar vacio";
        }
        else if (trainingM.email.characters.count == 0){
            status = false
            responseError = "El email no se puede enviar vacio";
        }
        else {
            status = true
        }
    }
    
    func validateHotel(hotelM :HotelModel) {
        if (hotelM.rooms.characters.count == 0){
            status = false
            responseError = "El tipo de habitación no se puede enviar vacio";
        }
        else if (hotelM.dateEntry.characters.count == 0){
            status = false
            responseError = "La fecha de entrada no se puede enviar vacia";
        }
        else if (hotelM.dateDeparture.characters.count == 0){
            status = false
            responseError = "La fecha de salida no se puede enviar vacia";
        }
        else if (hotelM.name.characters.count == 0){
            status = false
            responseError = "El nombre no se puede enviar vacio";
        }
        else if (hotelM.email.characters.count == 0){
            status = false
            responseError = "El email no se puede enviar vacio";
        }
        else if (hotelM.phone.characters.count == 0){
            status = false
            responseError = "El teléfono no se puede enviar vacio";
        }
        else {
            status = true
        }
    }

    func validateVet(vetM :VetModel) {
        if (vetM.service.characters.count == 0){
            status = false
            responseError = "El servicio no se puede enviar vacio";
        }
        else if (vetM.date.characters.count == 0){
            status = false
            responseError = "La fecha no se puede enviar vacia";
        }
        else if (vetM.dni.characters.count == 0){
            status = false
            responseError = "El Dni no se puede enviar vacio";
        }
        else if (vetM.phone.characters.count == 0){
            status = false
            responseError = "El teléfono no se puede enviar vacio";
        }
        else {
            status = true
        }
    }        
}
