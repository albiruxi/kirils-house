//
//  TaxiPresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

protocol PTaxiPresenter{
    func showSuccess()
    func showError(error: String)
    func showNumDogs(numDogs: [String]?)
}

class TaxiPresenter: NSObject {
    var delegate: PTaxiPresenter?
    var taxiComunicator: ServicesComunicator?
    var taxi = [TaxiModel]()
    var numDogs = ["1","2","3","4","5","6"]
    
    override init() {
        self.taxiComunicator = ServicesComunicator()
    }
    
    // MARK: Public Method
    
    func mainViewIsReady() {
        self.delegate?.showNumDogs(self.numDogs)
    }
    
    func sendTaxi(taxiM: TaxiModel) {
        
        let validator = ValidateServices()
        validator.validateTaxi(taxiM)
        if (validator.status) {
            self.sendServiceTaxi(taxiM)
        }
        else {
            self.delegate?.showError(validator.responseError!)
        }
    }
    
    // MARK : Private Method
    
    private func sendServiceTaxi(taxiM: TaxiModel){
        self.taxiComunicator?.sendContactTaxi(taxiM, completionHandler: { (response) in
            switch response {
            case .Success():
                self.delegate?.showSuccess()
            case .Error(let error):
                self.delegate?.showError(error.localizedDescription)
            }
        })
    }
}
