//
//  TaxiViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//


import UIKit

class TaxiViewController: UIViewController, PTaxiPresenter, UIPickerViewDelegate, UIPickerViewDataSource {
    
    //--OUTLET
    @IBOutlet var textFieldName: UITextField!
    @IBOutlet var textFieldAdress: UITextField!
    @IBOutlet var TextFieldPostalCode: UITextField!
    @IBOutlet var textFieldNumDogs: UITextField!
    @IBOutlet var textFieldDate: UITextField!
    @IBOutlet var textFieldSms: UITextField!
   
    //--VARIABLE
    var numDogs: [String]?
    var presenter: TaxiPresenter?
    var idService: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicialize()
    }
    
    func inicialize(){
        self.navigationItem.title = "Taxi"
        
        self.createBarButton()
        
        self.presenter = TaxiPresenter()
        self.presenter?.delegate = self
        self.presenter?.mainViewIsReady()
    }
    
    func createBarButton(){
        let barButton = UIBarButtonItem(title: "Enviar", style: .Plain , target: self, action: #selector(self.sendTaxi))
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    // MARK : Actions
    
    func sendTaxi(){
        let taxiM = TaxiModel()
        taxiM.name = self.textFieldName.text!
        taxiM.adress = self.textFieldAdress.text!
        taxiM.postalCode = self.TextFieldPostalCode.text!
        taxiM.numDogs = self.textFieldNumDogs.text!
        taxiM.date = self.textFieldDate.text!
        taxiM.sms = self.textFieldSms.text!
        
        self.presenter?.sendTaxi(taxiM)
    }
    
    // MARK: PTaxiPresenter
    
    func showNumDogs(numDogs: [String]?) {
        self.numDogs = numDogs
        self.createPicker()
    }
    
    func showSuccess() {
        let alertController = UIAlertController(title: "Alerta", message: "Reserva de taxi realizada correctamente", preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            self.navigationController?.popViewControllerAnimated(true)
        }
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true,  completion: nil)
    }
    
    func showError(error: String) {
        self.view.endEditing(true)
        let alertController = UIAlertController(title: "Alerta", message: error, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK : Private Method
    
    func createPicker() {
        // PICKER
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.backgroundColor = UIColor.whiteColor()
        self.textFieldNumDogs.inputView = pickerView
        
        // DATE PICKER
        let datePicker = UIDatePicker()
        datePicker.backgroundColor = UIColor.whiteColor()
        self.textFieldDate.inputView = datePicker
        datePicker.addTarget(self, action: #selector(self.datePickerValueChanged), forControlEvents: UIControlEvents.ValueChanged)
        
    }

    // MARK : PICKER DATE
    
    func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        self.textFieldDate.text = dateFormatter.stringFromDate(sender.date)
        self.textFieldDate.resignFirstResponder()
    }
    
    // MARK: PickerView
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.numDogs!.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.numDogs![row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let numDogsTemp = numDogs![row]
        self.textFieldNumDogs.text = numDogsTemp
        self.textFieldNumDogs.resignFirstResponder()
    }
}











