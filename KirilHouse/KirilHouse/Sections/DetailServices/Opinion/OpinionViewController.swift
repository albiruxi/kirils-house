//
//  OpinionViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit
import Social


class OpinionViewController: UIViewController, POpinionPresenter{
    
    //--OUTLET
    @IBOutlet var textFieldComent: UITextField!
  
    //--VARIABLE
    var idServices: String?
    var presenter: OpinionPresenter?
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicialize()

    }
    
    func inicialize(){
        self.navigationItem.title = "Opiniones"
        
        self.createBarButton()
        
        self.presenter = OpinionPresenter()
        self.presenter?.delegate = self
        self.presenter?.mainViewIsReady()

    }
    
    func createBarButton() {
        let barButton = UIBarButtonItem(title: "Enviar", style: .Plain , target: self, action: #selector(self.sendOpinion))
        self.navigationItem.rightBarButtonItem = barButton
    }
        
    func sendOpinion() {
        let opinionM = OpinionModel()
        opinionM.opinion = self.textFieldComent.text!
      
        self.presenter?.sendOpinion(opinionM)
 
    }
    
    // MARK: ACTIONS
    
    @IBAction func buttonTwitter(sender: AnyObject) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter){
            let tweetController = SLComposeViewController(forServiceType:  SLServiceTypeTwitter)
            tweetController.setInitialText(self.textFieldComent.text)
            self.presentViewController(tweetController, animated: true, completion: nil)
        }
        else{
            
            let alert = UIAlertController(title: "Cuenta", message: "Por favor logeate en Twitter", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            alert.addAction(UIAlertAction(title: "Ajustes", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) in
                let settingsURL = NSURL (string: UIApplicationOpenSettingsURLString)
                if let url = settingsURL{
                    UIApplication.sharedApplication().openURL(url)
                }
    
            }))
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }

    @IBAction func buttonFacebook(sender: AnyObject) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook){
            let facebookController = SLComposeViewController(forServiceType:  SLServiceTypeFacebook)
            facebookController.setInitialText(self.textFieldComent.text)
            self.presentViewController(facebookController, animated: true, completion: nil)
        }
        else{
            
            let alert = UIAlertController(title: "Cuenta", message: "Por favor logeate en Facebook", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            alert.addAction(UIAlertAction(title: "Ajustes", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) in
                let settingsURL = NSURL (string: UIApplicationOpenSettingsURLString)
                if let url = settingsURL{
                    UIApplication.sharedApplication().openURL(url)
                }
                
            }))
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
        
    // MARK: POpinionPresenter
    
    func showSuccess() {
        let alertController = UIAlertController(title: "Alerta", message: "Tu opinión se ha enviado correctamente", preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            self.navigationController?.popViewControllerAnimated(true)
        }
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true,  completion: nil)
    }
    
    
    func showError(error: String) {
        self.view.endEditing(true)
        let alertController = UIAlertController(title: "Alerta", message: error, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    func showOpinion(opinion: OpinionModel){
        self.textFieldComent.text = opinion.opinion
    }
    
}

 