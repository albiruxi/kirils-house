//
//  OpinionPresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 18/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit


protocol POpinionPresenter{
    func showSuccess()
    func showError(error: String)
    func showOpinion(opinion: OpinionModel)
}

class OpinionPresenter: NSObject {
    var delegate: POpinionPresenter?
    var opinionComunicator: ServicesComunicator?
    var opinion = OpinionModel()
    
    override init() {
        self.opinionComunicator = ServicesComunicator()
        
    }
    
    // MARK : Public method
    
    func mainViewIsReady() {
        self.delegate?.showOpinion(self.opinion)
    }
    
    func sendOpinion(opinionM: OpinionModel){
        
        let validator = ValidateServices()
        validator.validateOpinion(opinionM)
        if (validator.status){
            self.sendServiceOpinion(opinionM)
        }
        else{
            self.delegate?.showError(validator.responseError!)
        }
    }
    
    func sendServiceOpinion(opinionM: OpinionModel){
        self.opinionComunicator?.sendContactOpinion(opinionM, completionHandler: { (response) in
            switch response {
            case .Success():
                self.delegate?.showSuccess()
            case .Error(let error):
                self.delegate?.showError(error.localizedDescription)
            }
        })
    }
}
 