//
//  KennelViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 5/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class KennelViewController: UIViewController, PKennelPresenter, UITableViewDelegate {
    
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var botonMenu: UIBarButtonItem!
    
    //--VARIABLE--//
    var presenter: KennelPresenter?
    var listItemMenu: [KennelModel]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.prepareMenu()
        
        self.initializeView()
    }
    
    func initializeView() {
        self.presenter = KennelPresenter()
        self.presenter?.delegate = self
        self.presenter?.mainViewIsready()
    }
    
    // MARK: Private Method
    
    func prepareMenu() {
        if self.revealViewController() != nil{
            botonMenu.target = self.revealViewController()
            botonMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    // MARK : PKennelPresenter
    
    func showKennels(listKennels: [KennelModel]) {
        self.listItemMenu = listKennels
        self.tableView.reloadData()
    }
    func showSpinner(show: Bool) {
    }
    
    // MARK : Table Delegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listItemMenu!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let kennelModel = self.listItemMenu![indexPath.row]
        let cell:KennelCell = tableView.dequeueReusableCellWithIdentifier("kennelCell") as! KennelCell!
        cell.titleLabel.text = kennelModel.title
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let kennelModel = self.listItemMenu![indexPath.row]        
        
        if (kennelModel.type == "Stay") {
            let vc = storyboard.instantiateViewControllerWithIdentifier("StaysViewController") as! StaysViewController
            vc.idKennel = kennelModel.idKennel
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if (kennelModel.type == "Play") {
            var vc = UIViewController()
            vc = storyboard.instantiateViewControllerWithIdentifier("PlayViewController") as! PlayViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            var vc = UIViewController()
            vc = storyboard.instantiateViewControllerWithIdentifier("RatesViewController") as! RatesViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}
