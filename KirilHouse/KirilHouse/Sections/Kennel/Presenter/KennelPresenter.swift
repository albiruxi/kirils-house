//
//  KennelPresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 4/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit


protocol PKennelPresenter{
    func showKennels(listKennels:[KennelModel])
    func showSpinner(show:Bool)
    
}


class KennelPresenter: NSObject {
    var delegate : PKennelPresenter?
    var listKennel = [KennelModel]()
    
    override init() {
        super.init()
    }
    
    // MARK : Public Method
    
    func  mainViewIsready() {        
        self.createMenu()
        self.delegate?.showKennels(self.listKennel)
    }
    
    // MARK : Private Method
    
    func createMenu() {
        self.delegate?.showSpinner(true)
        let kennelMenu1 = KennelModel()
        kennelMenu1.title = "Estancias de verano"
        kennelMenu1.idKennel = "summer"
        kennelMenu1.type = "Stay"
        let kennelMenu2 = KennelModel()
        kennelMenu2.title = "Estancias de invierno"
        kennelMenu2.idKennel = "winter"
        kennelMenu2.type = "Stay"
        let kennelMenu3 = KennelModel()
        kennelMenu3.title = "Junior Suite"
        kennelMenu3.idKennel = "juniorSuite"
        kennelMenu3.type = "Stay"
        let kennelMenu4 = KennelModel()
        kennelMenu4.title = "Estancias razas mini"
        kennelMenu4.idKennel = "miniRaces"
        kennelMenu4.type = "Stay"
        let kennelMenu5 = KennelModel()
        kennelMenu5.title = "Estancias razas grandes"
        kennelMenu5.idKennel = "bigRaces"
        kennelMenu5.type = "Stay"
        let kennelMenu6 = KennelModel()
        kennelMenu6.title = "Zona de recreo"
        kennelMenu6.idKennel = "play"
        kennelMenu6.type = "Play"
        let kennelMenu7 = KennelModel()
        kennelMenu7.title = "Tarifas"
        kennelMenu7.idKennel = "rate"
        kennelMenu7.type = "Rates"
        
        self.listKennel = [kennelMenu1, kennelMenu2, kennelMenu3, kennelMenu4, kennelMenu5, kennelMenu6, kennelMenu7]
    }
}


