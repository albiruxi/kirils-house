//
//  PlayCell.swift
//  KirilHouse
//
//  Created by ALBA VILA on 15/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class PlayCell: UITableViewCell {
    @IBOutlet var titleLabelPlay: UILabel!
    @IBOutlet var imagePlay: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
