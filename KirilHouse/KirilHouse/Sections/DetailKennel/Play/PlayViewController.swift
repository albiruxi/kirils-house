//
//  PlayViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 7/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class PlayViewController: UIViewController, UITableViewDelegate,PPlayPresenter {

    //--Outlet---
    @IBOutlet var tableView: UITableView!

    //--Variable--
    var presenter: PlayPresenter?
    var listPlay = [PlayModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicialize()
    }
    
    // MARK : PPlayPresenter
    
    func inicialize() {
        self.presenter = PlayPresenter()
        self.presenter?.delegate = self
        self.presenter?.mainViewIsReady()
        
        self.navigationItem.title = "Zona de recreo"
    }
    
    // MARK : PPlayPresenter
    
    func showPlay(listPlay: [PlayModel]) {
        self.listPlay = listPlay
        self.tableView.reloadData()
    }
    
    // MARK : TableViewDelegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listPlay.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("playCell", forIndexPath: indexPath) as!
        PlayCell
        
        let playModel: PlayModel = self.listPlay[indexPath.row]
        cell.titleLabelPlay.text = playModel.titleLabelPlay
        cell.imagePlay.sd_setImageWithURL(NSURL(string: playModel.imagePlay))
        cell.titleLabelPlay.numberOfLines = 0

        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 221
    }
}

    

