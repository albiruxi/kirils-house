//
//  PlayPresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 7/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit


protocol PPlayPresenter{
    func showPlay(play: [PlayModel])
}

class PlayPresenter{
    var delegate: PPlayPresenter?
    var playComunicator: KennelComunicator?
    var listPlay = [PlayModel]()
    
    init(){
        self.playComunicator = KennelComunicator()
    }
    
    // MARK : Public Method
    
   func mainViewIsReady() {
    self.playComunicator!.getListPlay({ (response) in
        switch (response) {
        case .Success(let listPlay):
            self.delegate?.showPlay(listPlay!)
            break
        case .Error(let error):
            NSLog(error.localizedDescription, "")
            break
        }
    })
    }
}

