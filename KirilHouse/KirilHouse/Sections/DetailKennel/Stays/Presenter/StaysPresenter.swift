//
//  StayslPresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 5/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit


protocol PStaysPresenter{
    func showStay(stay: StaysModel)
    func showSpinner(show:Bool)
}


class StaysPresenter {
    var delegate: PStaysPresenter?
    var communicator: KennelComunicator?

    init() {
        self.communicator = KennelComunicator()
    }
    
    // MARK : Public Method
    
    func mainViewIsReady(seasons: String) {
        self.delegate?.showSpinner(true)
        self.communicator?.getListStays(seasons, completionHandler: { (response) in
            self.delegate?.showSpinner(false)
            switch (response) {
            case .Success(let stay):
                self.delegate?.showStay(stay!)
                break
            case .Error(let error):
                NSLog(error.localizedDescription, "")
                break
            }
        })
    }
}
