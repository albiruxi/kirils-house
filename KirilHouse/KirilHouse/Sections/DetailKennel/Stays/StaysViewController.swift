//
//  StayslViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 5/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class StaysViewController: UIViewController, PStaysPresenter {
    
    
    //-- Outlet --
    @IBOutlet var titleDetail: UILabel!
    @IBOutlet var descriptionDetail: UILabel!
    @IBOutlet var photo: UIImageView!
    
    //-- Variable --
    var presenter: StaysPresenter?
    var idKennel: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicialize()
    }

    func inicialize() {
        self.descriptionDetail.numberOfLines = 0
        self.titleDetail.numberOfLines = 0
        self.presenter = StaysPresenter()
        self.presenter?.delegate = self
        self.presenter?.mainViewIsReady(idKennel!)
        
         self.navigationItem.title = "Estancias"
    }
    
    // MARK : PStaysPresenter
    
    func showStay(stay: StaysModel) {
        self.titleDetail.text = stay.title
        self.descriptionDetail.text = stay.descriptionStay
        self.photo.sd_setImageWithURL(NSURL(string: stay.photo))
    }
    
    func showSpinner(show: Bool) {
    }
}

