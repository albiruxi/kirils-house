//
//  RatesPresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 10/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit


protocol PRatesPresenter{
    func showRates(rates: RateModel)
}

class RatesPresenter: NSObject {
    var delegate: PRatesPresenter?
    var ratesComunicator: KennelComunicator?
    var rate = RateModel()
    
    override init() {
        self.ratesComunicator = KennelComunicator()
    }
    
    // MARK : Public Method
    
    func mainViewIsReady() {
        self.ratesComunicator?.getListRates({(response) in
            switch (response) {
            case .Success(let rates):
                self.delegate?.showRates(rates!)
                break
            case .Error(let error):
                NSLog(error.localizedDescription, "")
                break
            }
        })
    }
}



