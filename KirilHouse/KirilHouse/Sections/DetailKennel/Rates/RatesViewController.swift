//
//  RatesViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 10/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class RatesViewController: UIViewController, PRatesPresenter {
    
    
    
    //---OUTLET----
    @IBOutlet var photo: UIImageView!
    
    //---VARIABLE----
    var presenter: RatesPresenter?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicialize()    
    }
    
    func inicialize(){
        self.presenter = RatesPresenter()
        self.presenter?.delegate = self
        self.presenter?.mainViewIsReady()
        
        self.navigationItem.title = "Tarifas"
    }
    
    func showRates(rates: RateModel){
        self.photo.sd_setImageWithURL(NSURL(string: rates.photo))
        
        
    }

   
}


