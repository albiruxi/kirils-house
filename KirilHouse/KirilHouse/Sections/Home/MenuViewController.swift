//
//  HomeViewController.swift
//  Kiril's House
//
//  Created by ALBA VILA on 24/2/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class MenuViewController: BaseViewController, PHomePresenter {

    //-- IBOUTLET --
    @IBOutlet var botonMenu: UIBarButtonItem!
    @IBOutlet var imageDog: UIImageView!
    
    
    //-- Variable --
    var presenter: HomePresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //-- Init menu --
        self.prepareMenu()        
        
        self.presenter = HomePresenter()
        self.presenter?.delegate = self
        self.presenter!.mainViewIsReady()
    }
    
    // MARK: Private Method
    
    func prepareMenu() {
        if self.revealViewController() != nil{
            botonMenu.target = self.revealViewController()
            botonMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
 
    // MARK: PHomePresenter
    
    func showDogImage(dogImage: String) {
        self.imageDog.sd_setImageWithURL(NSURL(string: dogImage)) { (image: UIImage!, error: NSError!, cacheType: SDImageCacheType!, imageURL: NSURL!) in
                       
            self.imageDog.image = image
        }
    }
    
    func showSpinner(show: Bool) {
        self.spinner(show)
    }
}
