//
//  HomePresenter.swift
//  Kiril's House
//
//  Created by ALBA VILA on 27/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit


protocol PHomePresenter {
    func showDogImage(dogImage:String)
    func showSpinner(show:Bool)
}


class HomePresenter: NSObject {
    var delegate: PHomePresenter?
    var timer: NSTimer!
    var count: Int=0
    var homeCommunicator: HomeComunicator!
    var listImage = [DogsModel]()
    
    override init() {
        super.init()
        self.homeCommunicator = HomeComunicator()
    }
    
    // MARK: Public Method
    
    func mainViewIsReady() {
        self.getListDogs()
    }
    
    // MARK: Private Method
    private func getListDogs() {
        self.delegate?.showSpinner(true)
        self.homeCommunicator.getListDogs("Home") { response in
            switch (response) {
            case .Success(let listDogs):
                // list dogs --
                 self.listImage = listDogs!
                
                let dogModel = self.listImage[self.count]
                self.delegate?.showDogImage(dogModel.url)
                
                //-- Init Timer Roller --
                self.initTimer()
                
                break
            case .Error(let error):
                NSLog(error.localizedDescription, "")
                break
            }
            
            self.delegate?.showSpinner(false)
        }
    }
    
    private func initTimer() {
        self.timer = NSTimer.scheduledTimerWithTimeInterval(2.0,
                                                            target: self,
                                                            selector: #selector(self.tick),
                                                            userInfo: nil,
                                                            repeats: true)
        
    }
    
    // MARK: Carrusel Image 
    
    func tick() {
        self.count+=1
        if(self.count == self.listImage.count){
            self.count = 0
        }
                
        let dogModel = self.listImage[self.count]
        self.delegate?.showDogImage(dogModel.url)
    }
        
}
