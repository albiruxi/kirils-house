//
//  ContactViewController.swift
//  Kiril's House
//
//  Created by ALBA VILA on 7/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController, PContactPresenter {
    
    //--OUTLET
    @IBOutlet var menuBoton: UIBarButtonItem!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var matterTextField: UITextField!
    @IBOutlet var smsTextField: UITextField!
    
    
    //--VARIABLE---
    var presenter: ContactPresenter?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.prepareMenu()
        
        self.initializeView()
    }
    
    func initializeView() {
        self.navigationItem.title = "Contacto"
        self.createBarButton()
        self.presenter = ContactPresenter()
        self.presenter!.delegate = self
        
    }
    
    func createBarButton(){
        let barButton = UIBarButtonItem(title: "Enviar", style: .Plain , target: self, action: #selector(self.userDidSendContact))
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    
    //MARK: Actions
    
    func userDidSendContact() {
        let contactM = ContactModel()
        contactM.name = self.nameTextField.text!
        contactM.mail = self.emailTextField.text!
        contactM.phone = self.phoneTextField.text!
        contactM.matter = self.matterTextField.text!
        contactM.sms = self.smsTextField.text!
        
        self.presenter?.userDidSendContact(contactM)
    }

    // MARK: Private Method

    func prepareMenu() {
        if self.revealViewController() != nil{
            menuBoton.target = self.revealViewController()
            menuBoton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }


    // MARK : PContactPresenter
    func showSpinner(show:Bool){
        
    }
    func showError(error: String){
        self.view.endEditing(true)
        let alertController = UIAlertController(title: "Alerta", message: error, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    func showSucces(){
        let alertController = UIAlertController(title: "Email", message: "Enviado correctamente", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }

   
}
