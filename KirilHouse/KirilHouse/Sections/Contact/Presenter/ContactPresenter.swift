//
//  ContactPresenter.swift
//  Kiril's House
//
//  Created by ALBA VILA on 30/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

protocol PContactPresenter {
    func showSpinner(show:Bool)
    func showError (error: String)
    func showSucces()
}


class ContactPresenter: NSObject {
    var delegate: PContactPresenter?
    var contactCommunicator: ContactComunicator!
    var contact = [ContactModel]()
    
    override init() {
        super.init()
        self.contactCommunicator = ContactComunicator()
    }

    
    // MARK : Public Method
    
    func userDidSendContact(contactM: ContactModel) {
      
        let validateContact = ValidateContact()
        validateContact.validateContact(contactM)
        if (validateContact.status){
            self.sendContact(contactM)
        }
        else {
            self.delegate?.showError(validateContact.responseError!)
        }
  }
    
    
    // MARK : Private Method
    
    func sendContact(contact: ContactModel) {
        self.delegate?.showSpinner(true)
        self.contactCommunicator.sendContact(contact) { (response) in
            self.delegate?.showSpinner(false)
            switch response {
            case .Success:
                self.delegate?.showSucces()
            case .Error(let error):
                self.delegate?.showError(error.localizedDescription)
            }
        }
    }
}
