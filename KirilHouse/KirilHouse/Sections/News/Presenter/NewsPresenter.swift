//
//  NewsPresenter.swift
//  Kiril's House
//
//  Created by ALBA VILA on 28/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit


protocol PNewsPresenter {
    func showNews(listNews:[NewsModel])
}

class NewsPresenter {
    var delegate : PNewsPresenter?
    var newsComunicator: NewsComunicator?
    var listNews = [NewsModel]()
    
    init() {
      self.newsComunicator = NewsComunicator()       
    }
    
    // MARK: Public Method
    
    func mainViewIsReady(){
        self.newsComunicator?.getListNews({ (response) in
            switch (response) {
            case .Success(let listNews):
                self.delegate?.showNews(listNews!)
                break
            case .Error(let error):
                NSLog(error.localizedDescription, "")
                break
            }
        })
    }
}