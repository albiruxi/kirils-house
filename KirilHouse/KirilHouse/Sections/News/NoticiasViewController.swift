//
//  NoticiasViewController.swift
//  Kiril's House
//
//  Created by ALBA VILA on 10/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit


class NoticiasViewController: UIViewController, UITableViewDataSource, UITabBarDelegate, PNewsPresenter {

    //-- IBOUTLET --
    @IBOutlet var tableView: UITableView!
    @IBOutlet var botonMenu: UIBarButtonItem!    
    
     //-- Variable --
    var presenter: NewsPresenter?
    var listNews = [NewsModel]()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //-- Init menu --
        self.prepareMenu()
        
        self.initializeView()
    }
    
    // MARK : Initialize View
    
    func initializeView() {
        self.presenter = NewsPresenter()
        self.presenter?.delegate = self
        self.presenter?.mainViewIsReady()
        
        self.navigationItem.title = "Noticias"
    }
    
    func prepareMenu() {
        if self.revealViewController() != nil{
            botonMenu.target = self.revealViewController()
            botonMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
    }    
    
    // MARK : PNewsPresenter
    
    func showNews(listNews: [NewsModel]) {
        self.listNews = listNews
        self.tableView.reloadData()
    }
    
    // MARK : TableViewDelegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listNews.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("cellNews", forIndexPath: indexPath) as!
        NewsCustomCell
        
        let newsModel: NewsModel = self.listNews[indexPath.row]
        cell.titleNews.text = newsModel.titleNews
        cell.descriprionNews.text = newsModel.descriptionNews
        cell.imageNews.sd_setImageWithURL(NSURL(string: newsModel.imageNews))
        cell.titleNews.numberOfLines = 0
        cell.descriprionNews.numberOfLines = 0
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 221
    }
}
