//
//  NoticiasCustomCell.swift
//  Kiril's House
//
//  Created by ALBA VILA on 10/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class NewsCustomCell: UITableViewCell {

    @IBOutlet var imageNews: UIImageView!
    @IBOutlet var titleNews: UILabel!
    @IBOutlet var descriprionNews: UILabel!
       
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
