//
//  VideosViewController.swift
//  Kiril's House
//
//  Created by ALBA VILA on 17/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit


class VideosViewController: UIViewController, PVideosPresenter, UITableViewDataSource, UIWebViewDelegate {
   
    //-- IBOUTLET --
  
    @IBOutlet var tableViewVideos: UITableView!
    
    
    //-- Variable --
    var presenter: VideosPresenter?
    var listVideos = [VideosModel]()
    var idPark: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicialize()
    }
    
    func inicialize(){
        self.presenter = VideosPresenter()
        self.presenter?.delegate = self
        self.presenter!.mainViewIsReady()
        
         self.navigationItem.title = "Videos"
    }

    // MARK :  PVideosPresenter
    
    func showVideo(listVideos: [VideosModel]) {
        self.listVideos = listVideos
        self.tableViewVideos.reloadData()
    }
    
    // MARK : TableViewDelegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listVideos.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("videosCell", forIndexPath: indexPath) as!
        VideosCell
        let videosModel: VideosModel = self.listVideos[indexPath.row]
        cell.titleVideo.text = videosModel.titleVideo
        cell.titleVideo.numberOfLines = 0
        let url = NSURL (string: videosModel.url)
        let request = NSURLRequest(URL: url!)
        
        cell.WebView.loadRequest(request)
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 221
    }
}







