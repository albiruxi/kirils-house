//
//  VideosPresenter.swift
//  Kiril's House
//
//  Created by ALBA VILA on 30/4/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit


protocol PVideosPresenter {
    func showVideo(listVideos: [VideosModel])
}


class VideosPresenter {
    var delegate: PVideosPresenter?
    var videosCommunicator: VideosComunicator?
    var listVideos = [VideosModel]()
    
    init() {
        self.videosCommunicator = VideosComunicator()
    

    }
    
    // MARK: Public Method
    
    func mainViewIsReady(){
        self.videosCommunicator?.getListVideos({ (response) in
            switch (response) {
            case .Success(let listVideos):
                self.delegate?.showVideo(listVideos!)
                break
            case .Error(let error):
                NSLog(error.localizedDescription, "")
            }
        })
    }
}