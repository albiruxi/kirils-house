//
//  NormsViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 9/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class NormsViewController: UIViewController, PNormsPresenter {
    
    //--OUTLET
    @IBOutlet var imageNorms: UIImageView!
    
    
    //--VARIABLE---
    var presenter: NormsPresenter?
    var idPark: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicialize()
    }
    
    func inicialize(){
        self.presenter = NormsPresenter()
        self.presenter?.delegate = self
        self.presenter?.mainViewIsReady()
        
         self.navigationItem.title = "Normas"
    }

    // MARK : PNormsPresenter
    
    func showNorms(norms: NormsModel){
        self.imageNorms.sd_setImageWithURL(NSURL(string: norms.imageNorms))
        
    }
}

    

