//
//  NormsPresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 9/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit


protocol PNormsPresenter{
    func showNorms(norms: NormsModel)
}


class NormsPresenter {
    var delegate: PNormsPresenter?
    var normsComunicator: ParkComunicator?
    var norms = [NormsModel]()
    
    init() {
        self.normsComunicator = ParkComunicator()
    }
    
    // MARK : Public Method
    
    func mainViewIsReady(){
        self.normsComunicator?.getListNorms({(response) in switch (response){
        case .Success(let norms):
            self.delegate?.showNorms(norms!)
            break
        case .Error(let error):
            NSLog(error.localizedDescription, "")
            }
        })
    }
}
    

    

