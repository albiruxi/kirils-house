//
//  CalendarPresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 10/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

protocol PCalendarPresenter {
    func showCalendar(listCalendar: [CalendarModel])
}

class CalendarPresenter {
    var delegate: PCalendarPresenter?
    var calendarComunicator: ParkComunicator?
    var listCalendar = [CalendarModel]()
    
    init(){
        self.calendarComunicator = ParkComunicator()
    }
    
    func mainViewIsReady(){
        self.calendarComunicator?.getListCalendar({ (response) in switch (response) {
        case .Success(let listCalendar):
            self.delegate?.showCalendar(listCalendar!)
            break
        case .Error(let error):
            NSLog(error.localizedDescription, "")
            break
            }
        })
    }

}
