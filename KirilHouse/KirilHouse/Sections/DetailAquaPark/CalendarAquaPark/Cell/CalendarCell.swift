//
//  CalendarCell.swift
//  KirilHouse
//
//  Created by ALBA VILA on 19/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class CalendarCell: UITableViewCell {
    
    @IBOutlet var imageCalendar: UIImageView!
    @IBOutlet var imageRate: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
