//
//  CalendarCollectionViewCell.swift
//  KirilHouse
//
//  Created by ALBA VILA on 25/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class CalendarCollectionViewCell: UICollectionViewCell {    
    
    @IBOutlet var imageCalendar: UIImageView!
    @IBOutlet var imageRate: UIImageView!
    
}
