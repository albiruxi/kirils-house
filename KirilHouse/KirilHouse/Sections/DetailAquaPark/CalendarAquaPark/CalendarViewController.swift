//
//  DetailAquaParkViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 7/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class CalendarViewController: UIViewController, PCalendarPresenter, UICollectionViewDelegate {
    
    //--IBOUTLET---
    
   // @IBOutlet var tableView: UITableView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var pageController: UIPageControl!
    
    
    //-- Variable --
    var presenter: CalendarPresenter?
    var listCalendar = [CalendarModel]()
    var idPark: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicialize()
    }

    func inicialize(){
        self.presenter = CalendarPresenter()
        self.presenter?.delegate = self
        self.presenter?.mainViewIsReady()
            
        self.navigationItem.title = "Calendario"
        
        self.collectionView.invalidateIntrinsicContentSize()
        self.collectionView.pagingEnabled = true
    }
    
    // MARK : PCalendarPresenter
    
    func showCalendar(listCalendar: [CalendarModel]) {
        self.listCalendar = listCalendar
        self.collectionView.reloadData()
    }
    
    // MARK : Collection delegate
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return self.listCalendar.count
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let calendarModel = self.listCalendar[indexPath.section]
        
        let cellColl = collectionView.dequeueReusableCellWithReuseIdentifier("calendarCollectionCell", forIndexPath: indexPath)
        let cell = cellColl as! CalendarCollectionViewCell
        
        cell.imageCalendar.sd_setImageWithURL(NSURL(string: calendarModel.imageCalendar))
        cell.imageRate.sd_setImageWithURL(NSURL(string: calendarModel.imageRate))
        
        return cell
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(self.collectionView.frame.size.width, self.collectionView.frame.size.height - 70);
    }
}




