//
//  QuestionViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 10/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class QuestionsViewController: UIViewController, PQuestionsPresenter, UITableViewDelegate {

    
    //---OUTLET---
    @IBOutlet var tableView: UITableView!
    
    //---VARIABLE---
    var presenter: QuestionPresenter?
    var listQuestions = [QuestionModel]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicialize()

    }
    
    func inicialize(){
        self.presenter = QuestionPresenter()
        self.presenter?.delegate = self
        self.presenter?.mainViewIsReady()
        
         self.navigationItem.title = "Preguntas frecuentes"
        
        self.tableView.estimatedRowHeight = 68.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
    }
     // MARK : PQuestionPresenter
    
    func showQuestions(listQuestion: [QuestionModel]){
        self.listQuestions = listQuestion
        self.tableView.reloadData()

    }
    
    // MARK : Table delegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listQuestions.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       let questionModel = self.listQuestions[indexPath.row]
        let cell:QuestionCell = tableView.dequeueReusableCellWithIdentifier("questionCell") as! QuestionCell!
        cell.question.text = questionModel.question
        cell.answer.text = questionModel.answer
        cell.question.numberOfLines = 0
        cell.answer.numberOfLines = 0
        return cell
    }
}
