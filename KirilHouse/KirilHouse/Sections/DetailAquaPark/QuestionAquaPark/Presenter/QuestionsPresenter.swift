//
//  QuestionPresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 10/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

protocol PQuestionsPresenter {
    func showQuestions(listQuestion: [QuestionModel])
}

class QuestionPresenter {
    var delegate: PQuestionsPresenter?
    var questionComunicator: ParkComunicator?
    var listQuestions = [QuestionModel]()
    
    init(){
        self.questionComunicator = ParkComunicator()
    }
    
     // MARK : Public Method
    
    func mainViewIsReady(){
        self.questionComunicator!.getListQuestions({ (response) in
            switch (response) {
            case .Success(let listQuestions):
                self.delegate?.showQuestions(listQuestions!)
                break
            case .Error(let error):
                NSLog(error.localizedDescription, "")
                break
            }
        })
    }
}

