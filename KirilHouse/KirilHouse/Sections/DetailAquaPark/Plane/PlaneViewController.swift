//
//  PlaneViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 11/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class PlaneViewController: UIViewController, PPlanePresenter {
    
    //IBOUTLET
    @IBOutlet var imagePool: UIImageView!
    
    
    //---VARIABLE---
    var presenter: PlanePresenter?
    var idPark: String?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicialize()

    }
    func inicialize(){
        self.presenter = PlanePresenter()
        self.presenter?.delegate = self
        self.presenter?.mainViewIsReady()
        
         self.navigationItem.title = "Piscina"
    }
    
    // MARK : PPlanePresenter
    
    func showPlane(plane: PlaneModel) {
        self.imagePool.sd_setImageWithURL(NSURL(string: plane.photoPool))
    }

}
