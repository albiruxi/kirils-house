//
//  PlanePresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 11/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

protocol PPlanePresenter{
    func showPlane(plane: PlaneModel)
}

class PlanePresenter{
    var delegate: PPlanePresenter?
    var planeCommunicator: ParkComunicator?
    var plane = [PlaneModel]()
    
    init() {
        self.planeCommunicator = ParkComunicator()
    }
 
        // MARK : Public Method
        
        func mainViewIsReady(){
         self.planeCommunicator?.getListPlane({(response) in switch (response){
            case .Success(let plane):
                self.delegate?.showPlane(plane!)
                break
            case .Error(let error):
                NSLog(error.localizedDescription, "")
                }
            })
        }
}

