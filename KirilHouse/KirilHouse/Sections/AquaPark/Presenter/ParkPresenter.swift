//
//  ParkPresenter.swift
//  KirilHouse
//
//  Created by ALBA VILA on 7/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit


protocol PParkPresenter{
    func showPark(listPark:[AquaParkModel])
}

class ParkPresenter: NSObject {
    var delegate: PParkPresenter?
    var listPark = [AquaParkModel]()
    
    override init() {
        super.init()
    }
    
    // MARK : Public Method
    
    func  mainViewIsready() {
        self.createMenu()
        self.delegate?.showPark(self.listPark)
    }
    
    // MARK : Private Method
    
    func createMenu() {
        let parkMenu1 = AquaParkModel()
        parkMenu1.title = "Calendario y tarifas"
        parkMenu1.idPark = "Calendar"
        parkMenu1.type = "Calendar"
        let parkMenu2 = AquaParkModel()
        parkMenu2.title = "Normas y recomendaciones"
        parkMenu2.idPark = "Norms"
        parkMenu2.type = "Norms"
        let parkMenu3 = AquaParkModel()
        parkMenu3.title = "Preguntas frecuentes"
        parkMenu3.idPark = "Questions"
        parkMenu3.type = "Questions"
        let parkMenu4 = AquaParkModel()
        parkMenu4.title = "Plano instalaciones"
        parkMenu4.idPark = "Plane"
        parkMenu4.type = "Plane"
        let parkMenu5 = AquaParkModel()
        parkMenu5.title = "Videos"
        parkMenu5.idPark = "Videos"
        parkMenu5.type = "Videos"
        
        self.listPark = [parkMenu1, parkMenu2, parkMenu3, parkMenu4, parkMenu5]
    }
}


