//
//  ParkViewController.swift
//  KirilHouse
//
//  Created by ALBA VILA on 7/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class ParkViewController: UIViewController, UITableViewDelegate, PParkPresenter {
    
    
    @IBOutlet var tableView: UITableView! 
    @IBOutlet var botonMenu: UIBarButtonItem!
    
    //--VARIABLE--//
    
    var presenter: ParkPresenter?
    var listItemMenu: [AquaParkModel]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.prepareMenu()
        
        self.initializeView()       
    }
    
    func initializeView(){
        self.presenter = ParkPresenter()
        self.presenter!.delegate = self
        self.presenter!.mainViewIsready()
        
        self.navigationItem.title = "AquaPark"
    }
    
    // MARK: Private Method
    
    func prepareMenu() {
        if self.revealViewController() != nil{
            botonMenu.target = self.revealViewController()
            botonMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    // MARK : PParkPresenter
    
    func showPark(listPark: [AquaParkModel]) {
        self.listItemMenu = listPark
        self.tableView.reloadData()
    }
    
    // MARK : Table Delegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listItemMenu!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let parkModel = self.listItemMenu![indexPath.row]
        let cell:ParkCell = tableView.dequeueReusableCellWithIdentifier("parkCell") as! ParkCell!
        cell.titleLabel.text = parkModel.title
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let aquaParkModel = self.listItemMenu![indexPath.row]
        
        
        if (aquaParkModel.type == "Calendar") {
            let vc = storyboard.instantiateViewControllerWithIdentifier("CalendarViewController") as! CalendarViewController
            vc.idPark = aquaParkModel.idPark
            self.navigationController?.pushViewController(vc, animated: true)
        }

        
        if (aquaParkModel.type == "Norms") {
            let vc = storyboard.instantiateViewControllerWithIdentifier("NormsViewController") as! NormsViewController
            vc.idPark = aquaParkModel.idPark
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if (aquaParkModel.type == "Questions") {
            let vc = storyboard.instantiateViewControllerWithIdentifier("QuestionsViewController") as! QuestionsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if (aquaParkModel.type == "Plane") {
            let vc = storyboard.instantiateViewControllerWithIdentifier("PlaneViewController") as! PlaneViewController
            vc.idPark = aquaParkModel.idPark!
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if (aquaParkModel.type == "Videos") {
            let vc = storyboard.instantiateViewControllerWithIdentifier("VideosViewController") as! VideosViewController
            vc.idPark = aquaParkModel.idPark
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

