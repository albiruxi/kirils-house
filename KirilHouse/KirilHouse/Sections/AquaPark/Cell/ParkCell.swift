//
//  ParkCell.swift
//  KirilHouse
//
//  Created by ALBA VILA on 6/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import UIKit

class ParkCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
