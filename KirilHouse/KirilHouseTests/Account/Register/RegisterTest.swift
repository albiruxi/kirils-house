//
//  RegisterTest.swift
//  KirilHouse
//
//  Created by ALBA VILA on 11/5/16.
//  Copyright © 2016 ALBA VILA. All rights reserved.
//

import XCTest

@testable import KirilHouse


class RegisterTest: XCTestCase {
    
    var validate: ValidateUser?
    
    override func setUp() {
        super.setUp()
        
        self.validate = ValidateUser()
    }
    
    override func tearDown() {
        self.validate = nil
        
        super.tearDown()
    }
    
    // MARK : Tests
    
    func test_exists() {
        XCTAssertNotNil(self.validate)
    }
    
    func test_validate_success() {
        self.validate?.validate("alba@gmail.com", pass: "123456")
        XCTAssertTrue(self.validate!.status)
    }
    
    func test_validate_email_empty() {
        self.validate?.validate("", pass: "12345")
        XCTAssertFalse(self.validate!.status)
    }
    
    func test_validate_pass_empty() {
        self.validate?.validate("alba@gmail.com", pass: "")
        XCTAssertFalse(self.validate!.status)
    }
    
    func test_validate_text_error_exits() {
        self.validate?.validate("", pass: "12345")
        XCTAssert(self.validate!.responseError?.characters.count > 0)
    }
    
    func test_validate_correct_pass() {
        self.validate?.validate("alba@gmail.com", pass: "123456")
        XCTAssertTrue(self.validate!.status)
        XCTAssertNil(self.validate!.responseError)
    }
    
    func test_validate_incorrect_pass() {
        self.validate?.validate("alba@gmail.com", pass: "12345")
        XCTAssertFalse(self.validate!.status)
        XCTAssert(self.validate!.responseError?.characters.count >= 0)
    }
}
